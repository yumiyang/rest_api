import { createLogger, transports, format } from 'winston'

const logger = createLogger({
  transports: [
    new transports.Console({
      level: 'silly',
      format: format.combine(
        format.timestamp(),
        format.colorize(),
        format.printf(info => {
          if (info.stack) console.log(info.stack)
          if (typeof info.message === 'object') info.message = JSON.stringify(info.message)
          return `[${info.timestamp}] ${info.level}: ${info.message}`
        })
      )
    })
  ]
})

export default logger
