import nodemailer from 'nodemailer'
import aws from 'aws-sdk'
import Mail from 'nodemailer/lib/mailer'

let transporter: Mail

if (process.env.INTRANET === 'true') {
  transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: Number(process.env.SMTP_PORT),
    secure: false,
    ignoreTLS: true
    // auth: {
    //   user: process.env.SMTP_USER,
    //   pass: process.env.SMTP_PASS
    // }
  })
} else {
  aws.config.update({ region: 'us-west-2' })

  transporter = nodemailer.createTransport({
    SES: new aws.SES({
      apiVersion: '2012-10-17'
    })
  })

}

export default transporter
