import 'dotenv/config'
import pgPromise from 'pg-promise'
import moment from 'moment'

const pgp = pgPromise({
  capSQL: true,
  error: e => {
    console.log(e)
  }
})

pgp.pg.types.setTypeParser(1114, str => moment(str).format())
pgp.pg.types.setTypeParser(1184, str => moment(str).format())
pgp.pg.types.setTypeParser(1082, str => moment(str).format())

const db = pgp(process.env.DATABASE_URL)

const helpers = {
  skip: (c: any) => !c.exists || c.value === '' || c.value === null || c.value === 'null' || c.value === 'undefined',
  dateWrapper: (c: any) => {
    if (c.exists) return moment(c.value).format()
    return c.value
  }
}

const pgp_spec = pgPromise({
  receive(data: any) {
    camelizeColumnNames(data)
  },
  error: e => {
    console.log(e)
  }
})

const camelizeColumnNames = (data: any) => {
  const template = data[0];
  for (let prop in template) {
    let camel = pgp.utils.camelize(prop)
    if (!(camel in template)) {
      for (let i = 0; i < data.length; i++) {
        var d = data[i];
        d[camel] = d[prop]
        delete d[prop]
      }
    }
  }
}

const db_spec = pgp_spec(process.env.DATABASE_URL)

export { db, db_spec, pgp, helpers }
