import dotenv from 'dotenv'

process.env.NODE_ENV = process.env.NODE_ENV || 'development'

const envFound = dotenv.config({ path: `./.env.${process.env.NODE_ENV}` })
if (!envFound) throw new Error("Couldn't find .env file.")

export default {
  env: process.env.NODE_ENV,
  port: parseInt(process.env.PORT),
  host: process.env.HOST,
  db: {
    user: process.env.DB_USER,
    passwd: process.env.DB_PWD,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    name: process.env.DB_NAME
  },
  api: {
    prefix: '/spec'
  },
  awsLog: {
    accessKey: process.env.AWS_ACCESS_KEY,
    secretKey: process.env.AWS_SECRET_KEY,
    region: process.env.AWS_REGION,
    level: 'info',
    groupName: process.env.AWS_CLOUDWATCH_LOG_GROUP_NAME,
    streamName: process.env.AWS_CLOUDWATCH_LOG_STREAM_NAME
  },
  log: {
    level: process.env.LOG_LEVEL
  },
  jwt: {
    secret: process.env.JWT_SECRET
  },
  swagger: {
    host: process.env.HOST + ':' + process.env.PORT
  },
  aws: {
    region: process.env.AWS_REGION,
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_KEY,
    bucket: process.env.BUCKET_NAME
  },
  email: {
    address: process.env.EMAIL_ADDRESS
  },
  noti: {
    template: {
      requestOfCaseEnd: `[요청자명] 님이 [카테고리명] / [워크그룹명] / #Case[케이스아이디] 의 종료를 요쳥했습니다. 종료 요청에 동의하시겠습니까?`,
      agreementOfCaseEnd: `[카테고리명] / [워크그룹명] / #Case[케이스아이디] 의 종료일입니다. Case를 종료하시겠습니까?`,
      requestOfCaseDelete: `[요청자명] 님이 [카테고리명] / [워크그룹명] / #Case[케이스아이디] 의 Case 삭제를 요청했습니다. 승인하시겠습니까?`,
      caseMove: `[카테고리명] / [워크그룹명] / #Case[케이스아이디] 가 [이동 카테고리명] / [이동 워크그룹명] / #Case[케이스아이디] 로 이동되었습니다.`,
      invitationToManager: `[회원명] 님이 [카테고리명] / [워크그룹명] / #Case[케이스아이디] 의 담당자로 배정되었습니다.`,
      invitaionToParticipant: `[회원명] 님이 [카테고리명] / [워크그룹명] 의 #Case[케이스아이디] 에 초대되었습니다.`,
      caseEnd: `[카테고리명] / [워크그룹명] / #Case[케이스아이디] 의 종료되었습니다.`,
      caseDelete: `[카테고리명] / [워크그룹명] / #Case[케이스아이디] 의 삭제되었습니다.`,
    },
    type: {
      requestOfCaseEnd: 'case.end.request',
      agreementOfCaseEnd: 'case.end.agreement',
      requestOfCaseDelete: 'case.delete.request',
      caseMove: 'case.move',
      invitationToManager: 'manager.invite',
      invitaionToParticipant: 'participant.invite',
      sendProductFile: 'product.send',
      caseEnd: 'case.end',
      caseDelete: 'case.delete'
    }
  }
}
