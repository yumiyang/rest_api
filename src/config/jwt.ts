import jwt from 'jsonwebtoken'
import expressJwt from 'express-jwt'

export const sign = async (payload: any) => {
	return jwt.sign(payload, process.env.JWT_SECRET, { expiresIn: '1h' })
}

export const authorize = expressJwt({
	secret: process.env.JWT_SECRET,
	algorithms: ['HS256'],
})