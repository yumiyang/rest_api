import _ from 'lodash'
import { Server } from 'socket.io'

import { db } from '../config/db'

import Notification from './lib/notification'

export const checkPrefix = function (name: string) {
	const prefix = [name.substring(0, 1), name.substring(1)]

	if (prefix[0] === '@' || prefix[0] === '#') {
		return {
			type: prefix[0] === '@' ? 'private' : 'channel',
			name: prefix[1]
		}
	} else {
		throw new Error('적절한 채널/유저명이 아닙니다')
	}
}

export const caseParser = function (caseId: string | number) {
	if (typeof caseId === 'string') {
		if (caseId.charAt(0) === '#') {
			return caseId.split('_')[1]
		} else {
			return Number(caseId)
		}
	} else if (typeof caseId === 'number') {
		return caseId
	} else {
		throw new Error('Not correct caseId')
	}
}

export const getCaseUsers = async function (caseId: string | number): Promise<any[]> {
	caseId = caseParser(caseId)
	const users = await db.many(`
		SELECT
			user_id AS id
		FROM
			case_participant 
		WHERE
			case_id = '${caseId}'
		UNION
		SELECT
		manager as id
		FROM
			"case" 
		WHERE
			id = '${caseId}'
		UNION
		SELECT
			issuer as id
		FROM
			"case"
		WHERE
			id = '${caseId}'
		;
	`)

	return _.map(users, c => c.id)
}

export const getJoinedCases = async function (userId: string): Promise<number[]> {
	const cases = await db.manyOrNone(`
		SELECT
			case_id AS id
		FROM
			case_participant 
		WHERE
			user_id = '${userId}'
		UNION
		SELECT
			id 
		FROM
			"case" 
		WHERE
			issuer = '${userId}' OR
			manager = '${userId}'
	`)

	return _.map(cases, c => c.id)
}

export const getUserId = async function (email: string): Promise<string> {
	const user = await db.one(`
		SELECT
			id
		FROM
			"user"
		WHERE
			email = '${email}'
	`)

	return user.id
}

export const getUserInfo = async function (id: string): Promise<any> {
	const user = await db.one(`
		SELECT
			*
		FROM
			"user"
		WHERE
			id = '${id}'
	`)

	return user
}

export const getCaseInfo = async function (caseId: number): Promise<any> {
	const caseInfo = await db.one(`
		SELECT
			"case".*,
			"workgroup".id as workgroup_id,
			"workgroup".name as workgroup_name,
			"category".id as category_id,
			"category".name as category_name,
			"section".id as section_id,
			"section".name as section_name
		FROM
			"case"
		LEFT JOIN
			"workgroup"
				ON "case".workgroup_id = "workgroup".id
		LEFT JOIN
			"category"
				ON "category".id = "workgroup".category_id
		LEFT JOIN
			"section"
				ON "section".id = "category".section_id
		WHERE
			"case".id = '${caseId}'
	`)

	return caseInfo
}

export const getWorkgroupInfo = async function (workgroupId: number): Promise<any> {
	const workgroupInfo = await db.one(`
		SELECT
			"workgroup".*,
			"category".id as category_id,
			"category".name as category_name,
			"section".id as section_id,
			"section".name as section_name
		FROM
			"workgroup"
		LEFT JOIN
			"category"
				ON "category".id = "workgroup".category_id
		LEFT JOIN
			"section"
				ON "section".id = "category".section_id
		WHERE
			"workgroup".id = '${workgroupId}'
	`)

	return workgroupInfo
}

export const checkConnectedUser = async function (userId: string): Promise<boolean> {
	const connected = await db.oneOrNone(`
		SELECT
			*
		FROM
			"connection"
		WHERE
			user_id = '${userId}'
	`)

	return connected.length
}

export const getViewingUserSocketId = async function (userId: string, caseId: string | number) {
	caseId = caseParser(caseId)
	const connected = await db.oneOrNone(`
		SELECT
			*
		FROM
			"connection"
		WHERE
			user_id = '${userId}' AND
			last_case_id = '${caseId}'
	`)

	return connected
}

export const setLastCase = async function (socketId: string, caseId: string | number): Promise<void> {
	caseId = caseParser(caseId)
	await db.none(`
		UPDATE
			"connection"
		SET
			last_case_id = '${caseId}'
		WHERE
			socket_id = '${socketId}'
	`)
}

export const getSocketIdByCase = async function (caseId: string | number): Promise<string[]> {
	caseId = caseParser(caseId)
	const sockets = await db.manyOrNone(`
		SELECT 
			socket_id
		FROM
			"connection"
		WHERE
			user_id IN 
		(
			SELECT
				user_id AS ID 
			FROM
				case_participant 
			WHERE
				case_id = '${caseId}'
			UNION
			SELECT
				manager AS ID 
			FROM
				"case" 
			WHERE
				ID = '${caseId}'
			UNION
			SELECT
				issuer AS ID 
			FROM
				"case" 
			WHERE
				ID = '${caseId}'
		)
	`)

	return _.map(sockets, c => c.socket_id)
}

export const getSocketIdByUserId = async (userId: string) => {
	const user = await db.manyOrNone(`
		SELECT
			*
		FROM
			"connection"
		WHERE
			user_id = '${userId}'
	`)

	return _.map(user, u => u.socket_id)
}

export const clearConnection = async function (): Promise<void> {
	await db.none(`
		TRUNCATE TABLE
			"connection"
	`)
}

export const sendCaseNotification = async function (msg: any, io: Server, returnMe: boolean = false) {
	const users = await getCaseUsers(msg.case_id)
	users.map(async (user: any) => {
		if (user !== msg.from || returnMe) {
			const notification = new Notification(msg.id, 'notification', msg.from, user, msg.case_id, msg.content, msg.timestamp)
			await notification.save()
			notification.type = msg.type
			const userSockets = await getSocketIdByUserId(user)
			userSockets.map((u: string) => {
				io.to(u).emit('notification', notification)
			})
		}
	})
}

export const sendUserNotification = async function (msg: any, io: Server) {
	const notification = new Notification(msg.id, 'notification', msg.from, msg.to, msg.case_id, msg.content, msg.timestamp)
	await notification.save()
	notification.type = msg.type
	const userSockets = await getSocketIdByUserId(msg.to)
	userSockets.map((u: string) => {
		io.to(u).emit('notification', notification)
	})
}

export const sendCaseSystemMessage = async function (msg: any, io: Server) {
	const users = await getCaseUsers(msg.case_id)
	users.map(async (user: any) => {
		const notification = new Notification(msg.id, 'notification', msg.from, user, msg.case_id, msg.content, msg.timestamp)
		notification.type = msg.type
		const userSockets = await getSocketIdByUserId(user)
		userSockets.map((u: string) => {
			io.to(u).emit('system', notification)
		})
	})
}