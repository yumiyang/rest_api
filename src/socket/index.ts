import SocketIO from 'socket.io'
import redis from 'socket.io-redis'
import socketioJWT from 'socketio-jwt'
import http from 'http'
import Redis from 'ioredis'

import logger from '../config/logger'

import Message from './lib/message'
import Event from './lib/event'
import User from './lib/user'
import Vote from './lib/vote'

import { emailSender } from './push'
import { sendCaseNotification, sendUserNotification, getSocketIdByCase, setLastCase, getUserInfo, getCaseInfo, getCaseUsers, clearConnection, sendCaseSystemMessage, getWorkgroupInfo } from './utils'

export default async function initialize(server: http.Server) {
  const io = SocketIO(server)

  io.adapter(redis())
  io.origins('*:*')
  io.use(
    socketioJWT.authorize({
      secret: process.env.JWT_SECRET,
      handshake: true
    })
  )
  await clearConnection()

  io.on('connection', async (socket: Socket) => {
    logger.info(`Connect: '${socket.decoded_token.email}'`)

    const user = new User(socket.decoded_token.email, socket)
    await user.login()

    socket.email = socket.decoded_token.email

    socket.on('error', err => {
      logger.error(err)
    })

    socket.on('message', async data => {
      let userId: string

      if (user.id) {
        userId = user.id
      } else if (data.from) {
        userId = data.from
      } else {
        throw new Error('Login required')
      }

      const msg = new Message(data.to, userId, data.content)
      await msg.send(io)
    })

    socket.on('event', async data => {
      const event = new Event(data.to, data.type, socket.email, data.content, data.case_id)
      switch (data.type) {
        case 'userLogin':
          socket.emit('user', event)
          break
        case 'userLogout':
          socket.emit('user', event)
          break
        case 'caseViewChange':
          await setLastCase(socket.id, data.to)
          break
      }
    })

    socket.on('disconnect', async () => {
      await user.logout()
      logger.info(`Disconnect: '${socket.decoded_token.email}'`)
    })
  })

  const subscriber = new Redis()
  await subscriber.subscribe(['event', 'system'])

  subscriber.on('message', async (channel, message) => {
    if (channel === 'event') {
      const msg = JSON.parse(message)
      let caseInfo: any
      let userInfo: any
      let users: any

      const event = new Event(msg.to, msg.type, msg.from, msg.content, msg.case_id)
      await event.save()

      switch (msg.type) {
        case 'case.create':
          logger.info('================================= case.create ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 가 생성되었습니다.`
          const sockets = await getSocketIdByCase(msg.case_id)
          sockets.map(socket => io.sockets.connected[socket].join(msg.to))
          await sendCaseNotification(msg, io, true)
          if (msg.to !== msg.from) await emailSender(msg.type, msg)

          const createMsg = new Message(`#case_${msg.case_id}`, msg.from, 'create', msg.timestamp, 'system')
          await createMsg.send(io)
          break

        case 'case.update.progress':
          logger.info('================================= case.update.progress ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id}의 진척도가 ${msg.content.progress}% 달성되었습니다.`
          await sendCaseNotification(msg, io, true)
          break

        case 'case.delete.request':
          logger.info('================================= case.delete.request ==========================')
          userInfo = await getUserInfo(msg.from)
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${userInfo.name} 님이 ${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 의 삭제를 요쳥했습니다. 승인하시겠습니까?`
          msg.to = caseInfo.manager
          await sendUserNotification(msg, io)
          break

        case 'case.delete':
          logger.info('================================= case.delete ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 가 삭제되었습니다.`
          await sendCaseNotification(msg, io, true)
          break

        case 'case.restore':
          logger.info('================================= case.restore ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 가 복구되었습니다.`
          await sendCaseNotification(msg, io, true)
          break

        case 'case.end.request':
          logger.info('================================= case.end.request ==========================')
          userInfo = await getUserInfo(msg.from)
          caseInfo = await getCaseInfo(msg.case_id)
          users = await getCaseUsers(msg.case_id)
          msg.content = `${userInfo.name} 님이 ${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 의 종료를 요쳥했습니다. 종료 요청에 동의하시겠습니까?`
          await sendCaseNotification(msg, io)
          users.map(async (user: any) => {
            const vote = new Vote(user, msg.case_id, user === msg.from ? true : null)
            await vote.save()
          })
          break

        case 'case.end.agreement':
          logger.info('================================= case.end.agreement ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 의 종료일입니다. Case를 종료하시겠습니까?`
          msg.to = caseInfo.manager
          await sendUserNotification(msg, io)
          break

        case 'case.end':
          logger.info('================================= case.end ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 가 종료되었습니다.`
          await sendCaseNotification(msg, io, true)
          break

        case 'case.end.cancel':
          logger.info('================================= case.end.cancel ==========================')
          userInfo = await getUserInfo(msg.from)
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${userInfo.name} 님의 미동의로 ${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 가 종료가 취소되었습니다.`
          await sendCaseNotification(msg, io, true)
          break

        case 'case.move':
          logger.info('================================= case.move ==========================')
          logger.info(msg)
          caseInfo = await getCaseInfo(msg.case_id)
          let workgroupInfo = await getWorkgroupInfo(msg.content.beforeWorkgroupId)
          msg.content = `${workgroupInfo.category_name} / ${workgroupInfo.name} / #요청사항_${msg.case_id} 가 ${caseInfo.category_name} / ${caseInfo.workgroup_name} 로 이동되었습니다.`
          await sendCaseNotification(msg, io, true)
          await emailSender(msg.type, msg)
          break

        case 'case.participant.invite':
          logger.info('================================= case.participant.invite ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 의 참여자로 추가되었습니다.`
          await sendUserNotification(msg, io)

          const inviteMsg = new Message(`#case_${msg.case_id}`, msg.to, 'invite', msg.timestamp, 'system')
          await inviteMsg.send(io)

          await emailSender(msg.type, msg)
          break

        case 'case.manager.change':
          logger.info('================================= case.manager.change ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 의 처리자로 지정 되었습니다.`
          await sendUserNotification(msg, io)
          await emailSender(msg.type, msg)
          break

        case 'case.participant.leave':
          logger.info('================================= case.participant.leave ==========================')
          caseInfo = await getCaseInfo(msg.case_id)
          msg.content = `${caseInfo.category_name} / ${caseInfo.workgroup_name} / #요청사항_${msg.case_id} 의 참여자에서 삭제되었습니다.`
          await sendUserNotification(msg, io)

          const leaveMsg = new Message(`#case_${msg.case_id}`, msg.to, 'leave', msg.timestamp, 'system')
          await leaveMsg.send(io)
          break

        case 'product.send':
          logger.info('================================= case.send ==========================')
          await emailSender(msg.type, msg)
          break
      }
    } else {
      const msg = JSON.parse(message)
      await sendCaseSystemMessage(msg, io)
    }
  })

  logger.info('RT Socket Initialize Success')
}
