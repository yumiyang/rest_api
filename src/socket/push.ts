import fs from 'fs'
import path from 'path'
import handlebars from 'handlebars'
import _ from 'lodash'

import transporter from '../config/email'
import logger from '../config/logger'
import { getUserInfo, getCaseInfo } from './utils'

const caseInvite = async (event: any) => {
  const data: any = {}
  const requesterInfo = await getUserInfo(event.from)
  const receiverInfo = await getUserInfo(event.to)
  const caseInfo = await getCaseInfo(event.case_id)

  const htmlTemplate = fs.readFileSync(path.join(process.cwd(), 'templates', 'invitation.html'), 'utf-8')
  const template = handlebars.compile(htmlTemplate)

  data.case = caseInfo
  data.requester = requesterInfo
  data.receiver = receiverInfo
  data.contents = {}
  data.caseUrl = `${process.env.WEB_URL}/workgroup/${caseInfo.workgroup_id}/${caseInfo.id}`

  if (event.type === 'case.participant.invite') {
    data.contents.subject = `${requesterInfo.name} 님이 요청사항의 참여자로 초대했습니다.`
    data.contents.body = ` #요청사항${caseInfo.id}의 참여자로 초대했습니다.`
  } else {
    data.contents.subject = `#요청사항${caseInfo.id} 의 처리자로 배정되었습니다.`
    data.contents.body = ` #요청사항${caseInfo.id}의 담당자로 배정했습니다.`
  }

  return {
    template: template(data),
    info: { subject: data.contents.subject, email: receiverInfo.email }
  }
}

const caseMove = async (event: any) => {
  const data: any = {}
  const requesterInfo = await getUserInfo(event.from)
  const caseInfo = await getCaseInfo(event.case_id)
  const receiverInfo = await getUserInfo(caseInfo.manager)

  const htmlTemplate = fs.readFileSync(path.join(process.cwd(), 'templates', 'invitation.html'), 'utf-8')
  const template = handlebars.compile(htmlTemplate)

  data.case = caseInfo
  data.requester = requesterInfo
  data.receiver = receiverInfo
  data.contents = {}
  data.caseUrl = `${process.env.WEB_URL}/workgroup/${caseInfo.workgroup_id}/${caseInfo.id}`

  data.contents.subject = `#요청사항${caseInfo.id} 이 이동되었습니다.`
  data.contents.body = event.content

  return {
    template: template(data),
    info: { subject: data.contents.subject, email: receiverInfo.email }
  }
}

const productSend = async (event: any) => {
  const data: any = {}
  data.contents = {}

  const requesterInfo = await getUserInfo(event.from)
  const caseInfo = await getCaseInfo(event.case_id)
  const productInfo = event.content

  const htmlTemplate = fs.readFileSync(path.join(process.cwd(), 'templates', 'file.html'), 'utf-8')
  const template = handlebars.compile(htmlTemplate)

  data.contents.subject = `${requesterInfo.name} 님이 파일을 전송했습니다.`

  const product: any = {}
  product.downloadUrl = `${process.env.API_URL}/rest/file/${productInfo.id}/download`
  product.name = productInfo.originalname

  data.product = product
  data.case = caseInfo
  data.requester = requesterInfo

  return {
    template: template(data),
    info: { subject: data.contents.subject, email: event.to }
  }
}

export const emailSender = async (type: string, event: any) => {
  console.log('emailSender')
  let result: any

  switch (type) {
    case 'case.create':
    case 'case.participant.invite':
    case 'case.manager.change':
      result = await caseInvite(event)
      break
    case 'case.move':
      result = await caseMove(event)
      break
    case 'product.send':
      result = await productSend(event)
      break
  }

  try {
    transporter.sendMail({
      from: process.env.SUPPORT_EMAIL_ADDRESS,
      to: result.info.email,
      subject: result.info.subject,
      html: result.template
    })
  } catch (e) {
    logger.error(e.message)
  }
}
