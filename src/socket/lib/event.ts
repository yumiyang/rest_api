import moment from 'moment'
import Redis from 'ioredis'
import { v4 as uuidv4 } from 'uuid'
import { db, pgp } from '../../config/db'
export default class Event {
	id: string
	type: string
	timestamp: string
	from: string
	to: string
	case_id: string
	content: string | null

	constructor(
		to: string,
		type: string,
		from: string,
		content: string,
		case_id: string,
		timestamp?: string
	) {
		this.id = uuidv4()
		this.type = type
		this.timestamp = moment(timestamp).format()
		this.from = from
		this.to = to
		this.case_id = case_id
		this.content = content
	}

	public save = async () => {
		await db.none(
			pgp.helpers.insert(this, ['id', 'type', 'timestamp', 'from', 'to', 'case_id', 'content'], 'event')
		)
	}

	public send = async () => {
		const pub = new Redis()
		pub.publish('event', JSON.stringify(this))
	}
}
