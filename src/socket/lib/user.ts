import moment from 'moment'

import { db } from '../../config/db'
import * as repo from '../../api/repositories/rt'

export default class User {
	connection: any
	id?: string
	email: string
	last_activity?: moment.Moment

	constructor(email: string, connection: any) {
		this.email = email
		this.connection = connection
	}

	public login = async () => {
		const user = await db.one(`
      SELECT
        *
      FROM
        "user"
      WHERE
        "email" = '${this.email}'
    `)

		this.id = user.id

		await db.none(`
      UPDATE
        "user"
      SET
        connected = true,
        last_login = NOW()
      WHERE
        id = '${user.id}'
		`)

		await db.none(`
			INSERT INTO
				"connection"
				(user_id, socket_id, timestamp)
			VALUES
				('${user.id}', '${this.connection.id}', '${moment().format()}')
		`)

		const case_ids = await repo.getJoinedCases(<string>user.id)

		if (case_ids.length > 0 && case_ids[0] !== undefined) {
			this.connection.join(case_ids.map(id => `#case_${id}`))
		}

		this.connection.broadcast.emit('user', { email: this.email, status: 'login' })
	}

	public logout = async () => {
		if (!this.id) {
			throw new Error('Login 먼저!')
		}

		await db.none(`
      UPDATE
        "user"
      SET
        connected = false
      WHERE
        id = '${this.id}'
		`)

		await db.none(`
			DELETE FROM
				"connection"
			WHERE
				socket_id = '${this.connection.id}'
		`)

		this.connection.broadcast.emit('user', { email: this.email, status: 'logout' })
	}
}
