import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'
import _ from 'lodash'

import { db, pgp } from '../../config/db'

export default class Notification {
  id: string
  type: string
  timestamp: string
  from: string
  to: string
  case_id: string
  content: string
  vote_id?: string
  modified?: boolean
  deleted?: boolean

  constructor(
    id: string,
    type: 'message' | 'notification',
    from: string,
    to: string,
    case_id: string,
    content: string,
    timestamp?: string
  ) {
    this.id = id || uuidv4()
    this.type = type
    this.from = from
    this.to = to
    this.case_id = case_id
    this.timestamp = moment(timestamp).format()
    this.content = content
  }

  public save = async () => {
    await db.none(
      pgp.helpers.insert(this, ['id', 'type', 'timestamp', 'from', 'to', 'case_id', 'content'], 'notification')
    )
  }
}
