import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'
import _ from 'lodash'
import { Server } from 'socket.io'

import { db, pgp } from '../../config/db'
import { getSocketIdByUserId, getCaseUsers } from '../utils'
import Notification from './notification'

export default class Message {
	id: string
	type: string
	timestamp: string
	from: string
	to: string
	content: string
	saved: boolean

	constructor(
		to: string,
		from: string,
		content: string,
		timestamp?: string,
		type: string = 'message'
	) {
		this.id = uuidv4()
		this.timestamp = moment(timestamp).format()
		this.from = from
		this.to = to
		this.content = content
		this.type = type
		this.saved = false
	}

	public save = async () => {
		await db.none(
			pgp.helpers.insert(this, ['id', 'type', 'timestamp', 'from', 'to', 'content'], 'message')
		)
		this.saved = true
	}

	public send = async (socket: Server) => {
		if (!this.saved) await this.save()

		const caseUsers = await getCaseUsers(this.to)
		caseUsers.map(async cu => {
			const connected = await getSocketIdByUserId(cu)
			if (connected.length > 0) {
				connected.map(async c => {
					socket.to(c).emit('message', this)
				})
			} else {
				const noti = new Notification(this.id, 'message', this.from, cu, this.to, this.content, this.timestamp)
				await noti.save()
			}
		})
	}

	public saveNotification = async (userId: string) => {
		const noti = _.cloneDeep(this)
		noti.to = userId

		await db.none(
			pgp.helpers.insert(noti, ['id', 'type', 'timestamp', 'from', 'to', 'content'], 'notification')
		)
	}
}
