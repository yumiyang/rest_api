import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'
import { db, pgp } from '../../config/db'

export default class Event {
  id: string
  user_id: string
  case_id: string
  timestamp: string
  expired_at: string
  agreement: boolean | null

  constructor(
    user_id: string,
    case_id: string,
    agreement: boolean | null,
    expired_at?: string,
    timestamp?: string
  ) {
    this.id = uuidv4()
    this.user_id = user_id
    this.case_id = case_id
    this.agreement = agreement
    this.expired_at = moment(expired_at).format()
    this.timestamp = moment(timestamp).format()
  }

  public setAgreement = async (agreement: boolean) => {
    this.agreement = agreement
  }

  public save = async () => {
    await db.none(
      pgp.helpers.insert(this, ['id', 'user_id', 'case_id', 'timestamp', 'expired_at', 'agreement'], 'vote')
    )
  }
}
