import 'dotenv/config'

import express from 'express'
import http from 'http'
import moment from 'moment'

import logger from './config/logger'

import API from './api'
import RT from './socket'
import Scheduler from './scheduler'

async function startServer() {
  logger.info('서버 초기화 시작')
  logger.info(`현재 서버 시간은 ${moment().format()} 입니다.`)

  const app = express()
  const server = http.createServer(app)

  await API({ app })
  await RT(server)
  await Scheduler()

  server.listen(process.env.PORT, () => {
    logger.info(`Server listening on port: ${process.env.PORT}`)
  }).on('error', (err) => {
    logger.error(err.message)
    process.exit(1)
  })
}

startServer()
