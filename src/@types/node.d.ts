declare namespace NodeJS {
  interface ProcessEnv {
    JWT_SECRET: string
    PORT: string
    DATABASE_URL: string
    AWS_ACCESS_KEY_ID: string
    AWS_SECRET_ACCESS_KEY: string
    AWS_S3_BUCKET: string
    GROUPWARE_ID_SECRET: string
  }
}
