interface Socket extends SocketIO.Socket {
  user_id: string
  email: string
  decoded_token: {
    email: string
  }
}