import express from 'express'
import { unreadAll, unreadCheck, all } from '../../controllers/rt/notification.controller'

const router = express.Router({ mergeParams: true })

/**
 * @swagger
 * tags:
 *  name: Notification
 *  description: 알림
 * 
 * /rt/notification:
 *  get:
 *    summary: 전체 알림 불러오기
 *    tags: [Notification]
 *    parameters:
 *     - in: query
 *       name: limit
 *       type: number
 *       description: 페이징/갯수
 *     - in: query
 *       name: offset
 *       type: number
 *       description: 페이징/오프셋
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 * /rt/notification/unread:
 *  get:
 *    summary: 읽지 않은 알림 불러오기
 *    description: 7일치 + 읽지 않은 알림
 *    tags: [Notification]
 *    parameters:
 *     - in: query
 *       name: limit
 *       type: number
 *       description: 페이징/갯수
 *     - in: query
 *       name: offset
 *       type: number
 *       description: 페이징/오프셋
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 * /rt/notification/read:
 *  put:
 *    summary: 알림 읽음
 *    tags: [Notification]
 *    parameters:
 *    - in: body
 *      name: notifications
 *      type: object
 *      description: 알림 ID 배열
 *      example:
 *        ['notiId1(UUID)','notiId2(UUID)']
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 *
 */

router.route('/').get(all)
router.route('/unread').get(unreadAll)
router.route('/read').put(unreadCheck)

export default router
