import express from 'express'
import { authorize } from '../../../config/jwt'

import messageRoute from './message.route'
import notificationRoute from './notification.route'
import voteRoute from './vote.route'

const router = express.Router({ mergeParams: true })

router.use(authorize)

router.use('/message', messageRoute)
router.use('/notification', notificationRoute)
router.use('/vote', voteRoute)

export default router
