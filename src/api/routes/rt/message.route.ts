import express from 'express'
import { unreadAll, unreadCount, unreadCheck } from '../../controllers/rt/message.controller'

const router = express.Router({ mergeParams: true })

/**
 * @swagger
 * tags:
 *  name: Message
 *  description: 메세지
 * 
 * /rt/message/unread:
 *  post:
 *    summary: 읽지 않은 메세지 불러오기
 *    tags: [Message]
 *    parameters:
 *     - in: formData
 *       name: case_id
 *       type: number
 *       required: true
 *       description: 요청사항 ID
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 * /rt/message/unread/count:
 *  get:
 *    summary: 요청사항별 읽지 않은 메세지 갯수 불러오기
 *    tags: [Message]
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 * /rt/message/read:
 *  put:
 *    summary: 메세지 읽음 처리
 *    tags: [Message]
 *    parameters:
 *     - in: formData
 *       name: case_id
 *       type: number
 *       required: true
 *       description: 요청사항 ID
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 */

router.route('/unread').post(unreadAll)
router.route('/unread/count').get(unreadCount)
router.route('/read').put(unreadCheck)

export default router
