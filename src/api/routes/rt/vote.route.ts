import express from 'express'
import { voteOne, checkMe } from '../../controllers/rt/vote.controller'

const router = express.Router({ mergeParams: true })

/**
 * @swagger
 * tags:
 *  name: Vote
 *  description: 요청사항 종료 투표
 * 
 * /rt/vote:
 *  post:
 *    summary: 투표하기
 *    tags: [Vote]
 *    parameters:
 *     - in: formData
 *       name: case_id
 *       type: number
 *       description: 요청사항 ID
 *     - in: formData
 *       name: agreement
 *       type: boolean
 *       description: 동의/미동의
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 * /rt/vote/me:
 *  post:
 *    summary: 투표확인
 *    description: 요청사항에 투표했는지 확인
 *    tags: [Vote]
 *    parameters:
 *     - in: formData
 *       name: case_id
 *       type: number
 *       description: 요청사항 ID
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 */

router.route('/').post(voteOne)
router.route('/me').post(checkMe)

export default router
