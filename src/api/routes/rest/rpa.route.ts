import express from 'express'
import { readOne, readAll, updateOne, deleteOne, createOne } from '../../controllers/rest/rpa.controller'

const router = express.Router({ mergeParams: true })

router.route('/').get(readAll)
router.route('/').post(createOne)
router.route('/:rpaId').get(readOne)
router.route('/:rpaId').put(updateOne)
router.route('/:rpaId').delete(deleteOne)

export default router
