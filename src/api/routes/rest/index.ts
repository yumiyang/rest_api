import express from 'express'
import { authorize } from '../../../config/jwt'

import customerRoute from './customer.route'
import sectionRoute from './section.route'
import categoryRoute from './category.route'
import workgroupRoute from './workgroup.route'
import caseRoute from './case.route'
import fileRoute from './file.route'
import messageRoute from './message.route'
import notificationRoute from './notification.route'
import userRoute from './user.route'
import rpaRoute from './rpa.route'

const router = express.Router({ mergeParams: true })

router.use('/file', fileRoute)

router.use(authorize)

router.use('/customer', customerRoute)
router.use('/section', sectionRoute)
router.use('/category', categoryRoute)
router.use('/workgroup', workgroupRoute)
router.use('/case', caseRoute)
router.use('/message', messageRoute)
router.use('/notification', notificationRoute)
router.use('/user', userRoute)
router.use('/rpa', rpaRoute)

export default router
