import express from 'express'
import {
  readAll,
  readOne,
  createOne,
  updateOne,
  deleteOne
} from '../../controllers/rest/customer.controller'
import sectionRoute from './section.route'

const router = express.Router({ mergeParams: true })

router.use('/:customerId/section', sectionRoute)

router.route('/').get(readAll)
router.route('/:customerId').get(readOne)
router.route('/').post(createOne)
router.route('/:customerId').put(updateOne)
router.route('/:customerId').delete(deleteOne)!

export default router
