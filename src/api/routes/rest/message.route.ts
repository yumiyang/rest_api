import express from 'express'
import { readOne, readAll, updateOne, deleteOne, searchOne } from '../../controllers/rest/message.controller'

const router = express.Router({ mergeParams: true })

router.route('/search').get(searchOne)
router.route('/').get(readAll)
router.route('/:messageId').get(readOne)
router.route('/:messageId').put(updateOne)
router.route('/:messageId').delete(deleteOne)

export default router
