import express from 'express'
import { readByCase, check } from '../../controllers/rest/notification.controller'

const router = express.Router({ mergeParams: true })

router.route('/').post(readByCase)
router.route('/check').post(check)

export default router
