import express from 'express'
import {
  readAll,
  readOne,
  createOne,
  updateOne,
  deleteOne
} from '../../controllers/rest/workgroup.controller'

const router = express.Router({ mergeParams: true })

router.route('/').get(readAll)
router.route('/:workgroupId').get(readOne)
router.route('/').post(createOne)
router.route('/:workgroupId').put(updateOne)
router.route('/:workgroupId').delete(deleteOne)

export default router
