import express from 'express'
import {
  readAll,
  readOne,
  createOne,
  updateOne,
  deleteOne
} from '../../controllers/rest/section.controller'
import categoryRoute from './category.route'

const router = express.Router({ mergeParams: true })

router.use('/:sectionId/category', categoryRoute)

router.route('/').get(readAll)
router.route('/:sectionId').get(readOne)
router.route('/').post(createOne)
router.route('/:sectionId').put(updateOne)
router.route('/:sectionId').delete(deleteOne)!

export default router
