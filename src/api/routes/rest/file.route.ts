import express from 'express'
import { readAll, readOne, createOne, updateOne, deleteOne, downloadOne } from '../../controllers/rest/file.controller'
import upload from '../../global/upload'
import { authorize } from '../../../config/jwt'

const router = express.Router({ mergeParams: true })

router.route('/').get(authorize, readAll)
router.route('/:fileId').get(authorize, readOne)
router.route('/').post(authorize, upload.single('file'), createOne)
router.route('/:fileId').delete(authorize, deleteOne)
router.route('/').put(authorize, upload.single('file'), updateOne)
router.route('/:fileId/download').get(downloadOne)

export default router
