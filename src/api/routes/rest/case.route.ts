import express from 'express'
import { readAll, readOne, createOne, updateOne, deleteOne } from '../../controllers/rest/case.controller'
import fileRoute from './file.route'
import messageRoute from './message.route'

const router = express.Router({ mergeParams: true })

/**
 * @swagger
 * tags:
 *  name: Rest/Case
 *  description: 요청사항
 * 
 * definitions:
 *  Case:
 *    type: object
 *    required:
 *      - name
 *    properties:
 *      name:
 *        type: string
 *      status:
 *        type: string
 * 
 * /rest/case:
 *  get:
 *    summary: 요청사항 목록 불러오기
 *    tags: [Rest/Case]
 *    parameters:
 *     - in: formData
 *       name: workgroupId
 *       type: number
 *       description: 워크그룹 ID
 *     - in: query
 *       name: limit
 *       type: number
 *       description: 페이징/갯수
 *     - in: query
 *       name: offset
 *       type: number
 *       description: 페이징/오프셋
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 *  post:
 *    summary: 요청사항 추가
 *    tags: [Rest/Case]
 *    parameters:
 *     - in: formData
 *       name: body
 *       schema:
 *        $ref: '#/definitions/Case'
 *    
 */

router.use('/:caseId/file', fileRoute)
router.use('/:caseId/message', messageRoute)

router.route('/').get(readAll)
router.route('/:caseId').get(readOne)
router.route('/').post(createOne)
router.route('/:caseId').put(updateOne)
router.route('/:caseId').delete(deleteOne)

export default router
