import express from 'express'
import {
  readAll,
  readOne,
  createOne,
  updateOne,
  deleteOne
} from '../../controllers/rest/user.controller'

const router = express.Router({ mergeParams: true })

router.route('/').get(readAll)
router.route('/:userId').get(readOne)
router.route('/').post(createOne)
router.route('/:userId').put(updateOne)
router.route('/:userId').delete(deleteOne)

export default router
