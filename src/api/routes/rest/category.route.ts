import express from 'express'
import {
  readAll,
  readOne,
  createOne,
  updateOne,
  deleteOne
} from '../../controllers/rest/category.controller'
import workgroupRoute from './workgroup.route'

const router = express.Router({ mergeParams: true })

router.use('/:categoryId/workgroup', workgroupRoute)

router.route('/').get(readAll)
router.route('/:categoryId').get(readOne)
router.route('/').post(createOne)
router.route('/:categoryId').put(updateOne)
router.route('/:categoryId').delete(deleteOne)

export default router
