import express from 'express'

import authRoute from './auth.route'
import roleRoute from './role.route'

const router = express.Router({ mergeParams: true })

router.use('/', authRoute)
router.use('/role', roleRoute)

export default router
