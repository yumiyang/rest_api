import express from 'express'
import { authorize } from '../../../config/jwt'

import { readAll, updateAll, permissionCheck } from '../../controllers/auth/role.controller'

const router = express.Router()
router.use(authorize)

/**
 * @swagger
 * tags:
 *  name: Role
 *  description: 역할/권한
 * 
 * /auth/role:
 *  get:
 *    summary: 권한 조회
 *    tags: [Role]
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 *  put:
 *    summary: 권한 수정
 *    tags: [Role]
 *    parameters:
 *     - in: body
 *       name: role
 *       example:
 *        case_create:
 *          administrator: true
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Unauthorized
 *      500:
 *        description: Fail
 * 
 * /auth/role/check:
 *  post:
 *    summary: 권한 체크
 *    tags: [Role]
 *    parameters:
 *     - in: formData
 *       name: workgroup_id
 *       type: number
 *       description: 워크그룹 ID
 *     - in: FormData
 *       name: case_id
 *       type: number
 *       description: 요청사항 ID
 *    responses:
 *      200:
 *        description: Success
 *      500:
 *        description: Fail
 *      401:
 *        description: Unauthorized
 */

router.route('/').get(readAll)
router.route('/').put(updateAll)
router.post('/check', permissionCheck)

export default router