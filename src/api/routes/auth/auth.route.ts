import express from 'express'

import { authorize } from '../../../config/jwt'
import { Login, Register, Me, Sync, Link } from '../../controllers/auth/auth.controller'

const router = express.Router()

/**
 * @swagger
 * /auth/login:
 *  post:
 *    summary: 사용자 로그인
 *    tags: [Auth]
 *    security: []
 *    parameters:
 *     - in: formData
 *       name: email
 *       type: string
 *       required: true
 *       description: 사용자 email, gwid
 *     - in: formData
 *       name: password
 *       type: string
 *       required: true
 *       description: 사용자 password, md5 hash
 *    responses:
 *      200:
 *        description: Success
 *      500:
 *        description: Fail
 * 
 * /auth/register:
 *  post:
 *    summary: 사용자 등록
 *    description: 테스터 등록용, 화면 사용 X
 *    tags: [Auth]
 *    security: []
 *    parameters:
 *     - in: formData
 *       name: email
 *       type: string
 *       required: true
 *       description: email, gwid, gwno
 *     - in: formData
 *       name: password
 *       type: string
 *       required: true
 *       description: 비밀번호, md5 hash
 *     - in: formData
 *       name: name
 *       type: string
 *       required: true
 *       description: 이름
 *     - in: formData
 *       name: photo
 *       type: string
 *       description: 프로필 사진 URL
 *     - in: formData
 *       name: phone
 *       type: string
 *       description: 전화번호 
 *     - in: formData
 *       name: company
 *       type: string
 *       description: 회사명 
 *     - in: formData
 *       name: position
 *       type: string
 *       description: 직책 
 *     - in: formData
 *       name: department
 *       type: string
 *       description: 부서 
 *     - in: formData
 *       name: role
 *       type: string
 *       description: 권한
 *     - in: formData
 *       name: gwid
 *       type: string
 *       description: 그룹웨어 ID
 *     - in: formData
 *       name: gwid
 *       type: string
 *       description: 그룹웨어 No
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /auth/me:
 *  get:
 *    summary: 사용자 토큰 확인
 *    description: Authorize 필요
 *    tags: [Auth]
 *    parameters:
 *     - in: header
 *       name: Authorization
 *       type: string
 *       required: true
 *       description: JWT 키
 *    responses:
 *      200:
 *        description: Success
 *      401:
 *        description: Fail
 * 
 * /auth/sync:
 *  get:
 *    summary: 그룹웨어 동기화
 *    tags: [Auth]
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 */

router.post('/login', Login)
router.post('/register', Register)
router.get('/me', authorize, Me)
router.get('/sync', authorize, Sync)
router.post('/link', Link)

export default router
