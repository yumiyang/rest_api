import express from 'express'
const router = express.Router({ mergeParams: true })
import * as workGroupController from '../../controllers/spec/workgroup.controller'
/**
 * @swagger
 * /spec/workgroup/{groupId}/case/list:
 *  get:
 *    name: workgroup 케이스 리스트
 *    summary: workgroup 케이스 리스트
 *    tags: [Workgroup]
 *    parameters:
 *     - in: path
 *       name: groupId
 *       type: string
 *       required: true
 *       default: 1
 *       description: workgroup id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/workgroup/user/list:
 *  get:
 *    name: 전체 사용자 리스트
 *    summary: 전체 사용자 리스트
 *    tags: [Workgroup]
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/workgroup/list:
 *  get:
 *    name: 전체 워크그룹 리스트
 *    summary: 전체 워크그룹 리스트
 *    tags: [Workgroup]
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 */

// 워크 그룹별 케이스 리스트 및 세부 정보 조회
router.route('/:groupId/case/list/:caseId?')
    .get(
        workGroupController.getCaseInfoList
    )

router.route('/user/list')
    .get(
        workGroupController.getAllUserList
    )

router.route('/list')
    .get(
        workGroupController.getAllWorkgroupList
    )

export default router