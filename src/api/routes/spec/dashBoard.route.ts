import express from 'express'
const router = express.Router({ mergeParams: true })
import * as dashBoardController from '../../controllers/spec/dashBoard.controller'
/**
 * @swagger
 * /spec/dashboard/{status}:
 *  get:
 *    name: 대시보드
 *    summary: 대시보드
 *    tags: [Dashboard]
 *    parameters:
 *     - in: path
 *       name: status
 *       type: string
 *       required: true
 *       default: open
 *       description: 진행중(open) or 종료(close)
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 */

// 케이스 상태별 대시보드 조회
router.route('/:status')
    .get(
        dashBoardController.getDashBoard
    )

export default router