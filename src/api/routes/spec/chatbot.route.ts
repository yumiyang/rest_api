import express from 'express'
const router = express.Router({ mergeParams: true })
import * as chatBotController from '../../controllers/spec/chatbot.controller'
/**
 * @swagger
 * /spec/chatbot/answer/list:
 *  get:
 *    name: 챗봇 answer 리스트
 *    summary: 챗봇 answer 리스트
 *    tags: [ChatBot]
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * /spec/chatbot/answer/{type}:
 *  put:
 *    name: 챗봇 answer 수정
 *    summary: 챗봇 answer 수정
 *    tags: [ChatBot]
 *    parameters:
 *     - in: path
 *       name: type
 *       type: string
 *       required: true
 *       default: 1
 *       description: 수정 대상 챗봇 메시지 타입
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 */

// 챗봇 잔체 메시지 리스트
router.route('/answer/list')
    .get(
        chatBotController.getChatBotAnswerList
    )
// 챗봇 메시지 수정
router.route('/answer/:type')
    .put(
        chatBotController.updateChatBotAnswer
    )

export default router