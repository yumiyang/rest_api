import express from 'express'
import { authorize } from '../../../config/jwt'

import caseRoute from './case.route'
import chatbotRoute from './chatbot.route'
import dashboardRoute from './dashBoard.route'
import workgroupRoute from './workgroup.route'

const router = express.Router({ mergeParams: true })

router.use(authorize)

router.use('/case', caseRoute)
router.use('/chatbot', chatbotRoute)
router.use('/dashboard', dashboardRoute)
router.use('/workgroup', workgroupRoute)

export default router
