import express from 'express'
const router = express.Router({ mergeParams: true })
import * as caseController from '../../controllers/spec/case.controller'

/**
 * @swagger
 * /spec/case/{caseId}/user:
 *  get:
 *    name: 특정 케이스 멤버 리스트
 *    summary: 특정 케이스 멤버 리스트
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 *  post:
 *    name: 특정 케이스 멤버 추가
 *    summary: 특정 케이스 멤버 추가
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *     - in: body
 *       name: userList
 *       type: array
 *       required: true
 *       description: 사용자 정보 list
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/{caseId}/user/{userId}:
 *  delete:
 *    name: 특정 케이스 멤버 내보내기
 *    summary: 특정 케이스 멤버 내보내기
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *     - in: path
 *       name: userId
 *       type: string
 *       required: true
 *       default: b02d8c02-d5af-4489-ad21-8af009eaeed8
 *       description: user id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/{caseId}/file/{type}:
 *  get:
 *    name: 특정 케이스 파일 리스트 
 *    summary: 특정 케이스 파일 리스트 
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *     - in: path
 *       name: type
 *       type: string
 *       required: true
 *       default: 'attachment'
 *       description: 최종산출물(product) or 파일(attachment)
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/{caseId}/pin:
 *  get:
 *    name: 특정 케이스 메시지 북마크 리스트 
 *    summary: 특정 케이스 메시지 북마크 리스트
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 *  post:
 *    name: 특정 케이스 메시지 북마크 추가
 *    summary: 특정 케이스 메시지 북마크 추가
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *     - in: body
 *       schema:
 *         type: object
 *         properties:
 *           messageId:
 *            type: string
 *            example: 1
 *       required: true
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/file/{fileId}/download:
 *  get:
 *    name: 파일 다운로드
 *    summary: 파일 다운로드
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: fileId
 *       type: string
 *       required: true
 *       description: file id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/{caseId}:
 *  put:
 *    name: 특정 케이스 종료 or 삭제 or 이동 요청
 *    summary: 특정 케이스 종료 or 삭제 or 이동 요청
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       description: case id
 *     - in: query
 *       name: type
 *       type: string
 *       required: true
 *       description: end or delete or move
 *     - in: body
 *       schema:
 *         type: object
 *         properties:
 *           userId:
 *            type: string
 *            example: fac27f23-4bc7-4266-a1cc-da1d9eb80310
 *           workgroupId:
 *            type: string
 *            example: 12
 *       required: true
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/{caseId}/message/{messageId}:
 *  post:
 *    name: 특정 케이스 북마크 추가
 *    summary: 특정 케이스 북마크 추가
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *     - in: path
 *       name: messageId
 *       type: string
 *       required: true
 *       default: 1
 *       description: message id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 *  delete:
 *    name: 특정 케이스 북마크 삭제
 *    summary: 특정 케이스 북마크 삭제
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       default: 1
 *       description: case id
 *     - in: path
 *       name: messageId
 *       type: string
 *       required: true
 *       default: 1
 *       description: message id
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/{caseId}/file/{productId}/{version}/history:
 *  get:
 *    name: 파일 이전 버전 리스트
 *    summary: 파일 이전 버전 리스트
 *    tags: [Case]
 *    parameters:
 *     - in: path
 *       name: caseId
 *       type: string
 *       required: true
 *       description: case id
 *     - in: path
 *       name: productId
 *       type: string
 *       required: true
 *       description: 산출물 id
 *     - in: path
 *       name: version
 *       type: string
 *       required: true
 *       description: 최종 산출물 버전
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 * 
 * /spec/case/noti/history:
 *  get:
 *    name: 사용자별 알림 히스토리 리스트 (최근 10개 알림)
 *    summary: 사용자별 알림 히스토리 리스트
 *    tags: [Case]
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 *  put:
 *    name: 사용자별 알림 확인 flag 업데이트
 *    summary: 사용자별 알림 확인 flag 업데이트
 *    tags: [Case]
 *    responses:
 *      200:
 *        description: Success
 *      400:
 *        description: Fail
 */

// case 별 멤버 리스트 조회
// case 별 멤버 추가
router.route('/:caseId/user')
    .get(
        caseController.getCaseMemberList
    )
    .post(
        caseController.addCaseMember
    )
// case 별 특정 멤버 내보내기
router.route('/:caseId/user/:userId')
    .delete(
        caseController.deleteCaseMember
    )
// case 별 파일 리스트 조회 (product or attachment)
router.route('/:caseId/file/:type')
    .get(
        caseController.getFileList
    )
// case 별 북마크 list
router.route('/:caseId/pin')
    .get(
        caseController.getPinList
    )
// case 별 특정 북마크 추가 및 삭제
router.route('/:caseId/message/:messageId')
    .post(
        caseController.addPin
    )
    .delete(
        caseController.deletePin
    )
// case 별 특정 파일 다운로드
router.route('/file/:fileId/download')
    .get(
        caseController.downloadFile
    )
// case 종료 or 삭제 or 이동
router.route('/:caseId')
    .put(
        caseController.requestCase
    )
// case 별 파일 이전 버전 리스트 조회 (최종 산출물 버전 리스트)
router.route('/:caseId/file/:productId/:version/history')
    .get(
        caseController.getFileHistoryList
    )
// 사용자 별 알림 히스토리 리스트
// 알림 확인 flag 업데이트
router.route('/noti/history')
    .get(
        caseController.getNotiHistoryList
    )
    .put(
        caseController.updateNotiChecked
    )
// case 별 특정 파일 다운로드 (for email)
router.route('/file/:fileId/download/email')
    .get(
        caseController.downloadFile
    )
// 최종산출물 파일 email 전송
router.route('/:caseId/file/:fileId/email')
    .post(
        caseController.sendProductFile
    )

export default router