import _ from 'lodash'
import { db_spec, pgp } from '../../../config/db'

const getChatBotAnswerList = async () => {
    try {
        return await db_spec.any(
            `
        SELECT * FROM chatbot_answer ORDER BY id
        `
        )

    } catch (e) {
        throw e
    }
}

const updateChatBotAnswer = async (params: any, type: string) => {
    try {
        const query = await pgp.helpers.update(params, null, 'chatbot_answer') + `WHERE type='${type}' RETURNING *`
        const result = await db_spec.any(query)

        return result
    } catch (e) {
        throw e
    }
}

export {
    getChatBotAnswerList,
    updateChatBotAnswer
}