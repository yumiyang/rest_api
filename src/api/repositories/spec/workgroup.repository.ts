import { db_spec } from '../../../config/db'

const getWorkGroupCaseList = async (workgroupId: number, userId: string, caseId?: string) => {
    try {
        let caseArrary: any = [workgroupId]

        let CIDquery = `
            WITH CID as (
                SELECT id 
                FROM "case" 
                WHERE 
                    workgroup_id = ${caseArrary}
                    AND id 
                    IN (
                        SELECT case_id 
                        FROM case_participant 
                        WHERE user_id = '${userId}'
                    )
            ),
        `

        if (caseId) {
            CIDquery = `
            WITH CID as (
                SELECT id 
                FROM "case" 
                WHERE 
                    "case".id = '${caseId}'
            ),
            `
        }

        const result = await db_spec.any(
            `
        SET TIME ZONE 'Asia/Seoul';
        ${CIDquery} 
        M AS (
            SELECT 
                cu.case_id, count(cu.user_id) as member
            FROM 
                case_participant cu, "user" u
            WHERE 
                cu.user_id = u.id
                AND cu.case_id in (select * from CID)
            GROUP BY cu.case_id
        ), FP AS (
            SELECT 
                cf.case_id, count(f.id) as product_file
            FROM (
            	SELECT 
            		DISTINCT ON (product_id) product_id, case_id, file_id, filetype
        		FROM 
        			case_file 
				WHERE 
					case_id IN (select * from CID)
            ) AS cf, file f
            WHERE 
                cf.file_id = f.id
                AND cf.filetype = 'product'
            GROUP BY cf.case_id
        ), B AS (
            SELECT 
                case_id, count(message_id) as bookmark
            FROM 
                bookmark
            WHERE 
                case_id in (select * from CID)
            GROUP BY case_id
        ), FA AS (
            SELECT 
                cf.case_id, count(f.id) as attach_file
            FROM case_file cf, file f
            WHERE 
                cf.file_id = f.id
                AND cf.filetype = 'attachment'
             	AND cf.case_id in (select * from CID)
            GROUP BY cf.case_id
        )
        SELECT  
            C.id,
            C.workgroup_id,
            C.name,
            C.status,
            C.progress,
            to_char(C.start_date, 'YYYY/MM/DD') as start_date,
            to_char(C.end_date, 'YYYY/MM/DD') as end_date,
            to_char(C.target_date, 'YYYY/MM/DD') as target_date,
            to_char(C.request_date, 'YYYY/MM/DD') as request_date,
            C.end_date - current_date as diff_days,
            C.issuer,
            C.manager,
            coalesce(M.member, 0) as member_cnt,
            coalesce(FP.product_file, 0) as product_file_cnt,
            0 as related_case_cnt,
            coalesce(B.bookmark, 0) as bookmark_cnt,
            coalesce(FA.attach_file, 0) as attach_file_cnt,
            to_char(C.close_request_time, 'YYYY/MM/DD HH24:MI') as close_request_time,
            to_char(C.close_time, 'YYYY/MM/DD HH24:MI') as close_time,
            C.close_issuer,
            to_char(C.delete_request_time, 'YYYY/MM/DD HH24:MI') as delete_request_time,
            to_char(C.delete_time, 'YYYY/MM/DD HH24:MI') as delete_time,
            C.delete_issuer
        FROM "case" C
        	left join M on (C.id = M.case_id)
            left join FP on (C.id = FP.case_id)
            left join B on (C.id = B.case_id)
            left join FA on (C.id = FA.case_id)
        WHERE 
            C.id in (select * from CID)
            AND C.status != '삭제'
        ORDER BY C.id
        `)

        return result
    } catch (e) {
        throw e
    }
}

const getaAllUserList = async () => {
    try {
        const result = await db_spec.any(
            `
        SET TIME ZONE 'Asia/Seoul';
        SELECT 
            u.*, r.name_kor 
        FROM 
            "user" u left outer join "role" r on (u.role = r.name)
        ORDER BY
            u.name ASC
        `
        )

        return result
    } catch (e) {
        throw e
    }
}

const getCategoryName = async (workgroupId: number) => {
    try {
        const result = await db_spec.oneOrNone(
            `
        SET TIME ZONE 'Asia/Seoul';
        SELECT s.name || ' / ' || cg.name || ' / ' || w.name as category_nm 
        FROM "section" s, workgroup w, category cg
        WHERE 
            cg.section_id = s.id 
            AND w.category_id  = cg.id
            AND w.id = $1
        `
            ,
            [workgroupId]
            , (res) => res.categoryNm)

        return result
    } catch (e) {
        throw e
    }
}

const getAllWorkgoupList = async (userId: string) => {
    try {
        const result = await db_spec.any(
            `
        SET TIME ZONE 'Asia/Seoul';
        SELECT id, name 
        FROM workgroup 
        WHERE 
            id IN (
                SELECT 
                    DISTINCT ON (workgroup_id) workgroup_id FROM "case"
                WHERE 
                    ID in (
                        SELECT case_id FROM case_participant WHERE user_id = $1
                    )
            )
        `
            , [userId])

        return result
    } catch (e) {
        throw e
    }
}

const isManager = async (workgroupId: number, userId: string) => {
    try {
        const result = await db_spec.oneOrNone(
            `     
        SELECT 
            manager, submanager 
        FROM workgroup 
        WHERE 
            id = $1
            AND (
                manager = $2
                or submanager like '%${userId}%'
            )
        `
            , [workgroupId, userId])

        return result
    } catch (e) {
        throw e
    }
}

export {
    getWorkGroupCaseList
    , getaAllUserList
    , getCategoryName
    , getAllWorkgoupList
    , isManager
}