import _ from 'lodash'
import { db_spec, pgp } from '../../../config/db'
import config from '../../../config/spec'

const getCaseMemberList = async (caseId: number) => {
	try {
		const result = await db_spec.any(
			`
			SELECT
				"user".* 
			FROM
				case_participant
				LEFT JOIN "user" ON "user".ID = case_participant.user_id 
			WHERE
				case_id = '${caseId}'
		`)

		return result
	} catch (e) {
		throw e
	}
}

const addCaseMember = async (member: any) => {
	try {
		const query = await pgp.helpers.insert(member, ['case_id', 'user_id'], 'case_participant') + ' RETURNING *'
		const result = db_spec.any(query)

		return result
	} catch (e) {
		throw e
	}
}

const deleteCaseMember = async (caseId: number, userId: string) => {
	try {
		const result = await db_spec.any(
			`
		DELETE FROM case_participant 
		WHERE 
			case_id = $1 
			AND user_id = $2
		RETURNING *
		`
			, [caseId, userId])

		return result
	} catch (e) {
		throw e
	}
}

const getFileList = async (caseId: number, type: string) => {
	try {
		let query = ``
		if (type === 'product') {
			query =
				`
			SELECT 
				case_id, filetype, version, created_by
				, id, originalname, key, location, mimetype, f.size, created_at, product_id
			FROM 
			(
				SELECT 
					rank() over (partition by cf.product_id order by cf.version desc) as rn, 
					cf.*, 
					f.id,
					f.originalname,
					f.key,
					f.location,
					f.mimetype,
					f.size,
					to_char(f.created_at, 'YYYY/MM/DD HH24:MI') as created_at
				FROM 
					file f, case_file cf
				WHERE 
					f.id = cf.file_id
					AND cf.case_id = $1
					AND cf.filetype = $2
			) as F
			WHERE F.rn = 1
			ORDER BY created_at DESC
			`
		} else {
			query =
				`
			SELECT 
			cf.case_id, cf.filetype, cf.version, cf.created_by
			, f.id, f.originalname, f.key, f.location, f.mimetype, to_char(f.created_at, 'YYYY/MM/DD HH24:MI') as created_at, f.size, cf.product_id
			FROM 
				file f, case_file cf
			WHERE 
				f.id = cf.file_id
				AND cf.case_id = $1
				AND cf.filetype = $2
			ORDER BY created_at DESC
			`
		}

		const result = await db_spec.any(
			`
		SET TIME ZONE 'Asia/Seoul';
		${query}
		`
			, [caseId, type])

		return result
	} catch (e) {
		throw e
	}
}

const getPinList = async (caseId: number) => {
	try {
		const result = await db_spec.any(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT *
		FROM message
		WHERE 
			id IN (
				SELECT message_id FROM bookmark WHERE case_id = $1
			)
		`
			, [caseId])

		return result
	} catch (e) {
		throw e
	}
}

const getFileInfo = async (fileId: any) => {
	try {
		const result = await db_spec.oneOrNone(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT 
			originalname, location, id, key
		FROM file 
		WHERE id = $1
		`
			, [fileId])

		return result
	} catch (e) {
		throw e
	}
}

const addPin = async (caseId: number, messageId: string) => {
	try {
		const query = await pgp.helpers.insert({ case_id: caseId, message_id: messageId }, null, 'bookmark') + ' RETURNING *'
		const result = await db_spec.oneOrNone(query)

		return result
	} catch (e) {
		throw e
	}
}

const checkEndDate = async () => {
	try {
		const result = await db_spec.any(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT id FROM "case" WHERE end_date = current_date
		`
		)

		return result
	} catch (e) {
		throw e
	}
}

const getCaseInfo = async (caseId: number) => {
	try {
		const result = await db_spec.oneOrNone(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT 
			c.name as case_nm, w.name as workgroup_nm, cg.name as category_nm, s.name as section_nm
		FROM 
			"case" c, workgroup w, category cg, section s
		WHERE 
			c.workgroup_id  = w.id
			AND w.category_id = cg.id 
			AND cg.section_id = s.id
			AND c.id = $1
		`
			, [caseId])

		return result
	} catch (e) {
		throw e
	}
}

const getCaseMemberIdList = async (caseId: number) => {
	try {
		const result = await db_spec.map(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT id
		FROM "user"
		WHERE 
			id 
			IN (
				SELECT user_id 
				FROM case_participant
				WHERE case_id = $1
			)
		`
			, [caseId]
			, row => row.id)

		return result
	} catch (e) {
		throw e
	}
}

const deletePin = async (caseId: number, messageId: string) => {
	try {
		const result = await db_spec.oneOrNone(
			`
		DELETE FROM bookmark 
		WHERE 
			case_id = $1 
			AND message_id = $2 
		RETURNING *`
			, [caseId, messageId])

		return result
	} catch (e) {
		throw e
	}
}

const getFileHistoryList = async (caseId: number, productId: string, version: string) => {
	try {
		const result = await db_spec.any(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT 
			cf.case_id, cf.filetype, cf.version, cf.created_by
			, f.id, f.originalname, f.key, f.location, f.mimetype, f.size, to_char(f.created_at, 'YYYY/MM/DD HH24:MI') as created_at, cf.product_id
		FROM file f, case_file cf
		WHERE 
			f.id = cf.file_id
			AND cf.case_id = $1
			AND cf.product_id = $2
			AND cf.filetype = 'product'
			AND cf.version != $3
		ORDER BY cf.version DESC
		`
			, [caseId, productId, version])

		return result
	} catch (e) {
		throw e
	}
}

const getNotiHistoryList = async (userId: string) => {
	try {
		const result = await db_spec.any(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT 
			e.id, e.content, e.from, e.to, e.type, to_char(e.timestamp, 'YYYY/MM/DD HH24:MI') as timestamp, e.checked
		FROM "event" e 
		WHERE 
			e.to = $1
		ORDER BY e.timestamp desc
		LIMIT 10
		`
			, [userId])

		return result
	} catch (e) {
		throw e
	}
}

const updateCaseWorkgroupId = async (caseId: number, workgroupId: number) => {
	try {
		const workgroupInfo = await db_spec.oneOrNone(`
			SELECT
				manager
			FROM
				"workgroup"
			WHERE
				"id" = '${workgroupId}'
		`)

		const result = await db_spec.oneOrNone(`
			UPDATE
				"case"
			SET
				"workgroup_id" = '${workgroupId}',
				"manager" = '${workgroupInfo.manager}'
			WHERE
				"id" = ${caseId}
			RETURNING *
		`
		)

		await db_spec.none(`
			INSERT INTO
				case_participant
				("case_id", "user_id")
			VALUES
				('${caseId}', '${workgroupInfo.manager}')
			ON CONFLICT ON CONSTRAINT case_participant_pkey
			DO NOTHING;
		`)

		return result
	} catch (e) {
		throw e
	}
}

const updateNotiChecked = async (eventIdList: string[]) => {
	try {
		const result = await db_spec.any(
			`
		UPDATE "event" SET checked = true WHERE id IN ($1:csv) RETURNING *
		`
			, [eventIdList])

		return result
	} catch (e) {
		throw e
	}
}

const getCaseUserInfo = async (caseId: number, userId: string) => {
	try {
		const result = await db_spec.oneOrNone(
			`
		SET TIME ZONE 'Asia/Seoul';
		SELECT 
			s.name as section_nm,
			cg.name as category_nm,
			w.name as workgroup_nm,
			c.id, c.name, u.name as user_nm, u.email
		FROM "case" c, workgroup w, category cg, "section" s, "user" u
		WHERE 
			c.workgroup_id  = w.id 
			and w.category_id = cg.id 
			and cg.section_id = s.id
			and c.id = $1
			and u.id  = $2
		`
			, [caseId, userId])

		return result
	} catch (e) {
		throw e
	}
}

const isManager = async (caseId: number, userId: string) => {
	try {
		const result = await db_spec.oneOrNone(
			`
		SELECT manager
		FROM "case"
		WHERE 
			id = $1
			AND manager = $2
		`
			, [caseId, userId])

		return result
	} catch (e) {
		throw e
	}
}

const updateCaseStatus = async (caseId: number, type: string, issuer: string) => {
	try {
		let status = ``
		let caseQuery = ``
		switch (type) {
			case config.noti.type.caseEnd:
				status = '종료'
				break
			case config.noti.type.caseDelete:
				status = '삭제'
				break
			case config.noti.type.requestOfCaseEnd:
				status = '종료 요청'
				caseQuery =
					`
								, close_issuer = '${issuer}'
								, close_request_time = now()
								`
				break
			case config.noti.type.requestOfCaseDelete:
				status = '삭제 요청'
				caseQuery =
					`
								, delete_issuer = '${issuer}'
								, delete_request_time = now()
								`
		}

		const result = await db_spec.oneOrNone(
			`
		UPDATE "case" 
		SET 
			status = $1
			${caseQuery}
		WHERE 
			id = $2
		RETURNING *
		`
			, [status, caseId])

		return result
	} catch (e) {
		throw e
	}
}

export {
	getCaseMemberList
	, addCaseMember
	, deleteCaseMember
	, getFileList
	, getPinList
	, getFileInfo
	, addPin
	, checkEndDate
	, getCaseInfo
	, getCaseMemberIdList
	, deletePin
	, getFileHistoryList
	, getNotiHistoryList
	, updateCaseWorkgroupId
	, updateNotiChecked
	, getCaseUserInfo
	, isManager
	, updateCaseStatus
}