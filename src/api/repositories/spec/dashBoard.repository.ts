import { db_spec } from '../../../config/db'

const getDashBoard = async (userId: string, status: string) => {
    try {
        let statusQuery = status === 'open' ? ` NOT IN ('종료', '삭제', '삭제 요청')` : ` IN ('종료', '삭제 요청')`
        const result = await db_spec.any(
            `
        SET TIME ZONE 'Asia/Seoul';
        SELECT 
        	s.name AS section_nm,
        	cg.name AS category_nm,
        	w.name AS workgroup_nm,
            c.id, c.name, c.status, c.progress, c.issuer, c.manager, c.workgroup_id, 
            to_char(c.start_date, 'YYYY/MM/DD') AS start_date, 
            to_char(c.end_date, 'YYYY/MM/DD') AS end_date, 
            to_char(c.target_date, 'YYYY/MM/DD') AS target_date,
            to_char(c.request_date, 'YYYY/MM/DD') AS request_date
        FROM "case" c, workgroup w, category cg, "section" s
        WHERE 
        	c.workgroup_id  = w.id 
        	AND w.category_id = cg.id 
        	AND cg.section_id = s.id
            AND c.id 
            IN (SELECT case_id FROM case_participant WHERE user_id = $1)
            AND status ${statusQuery}
        `
            , [userId])

        return result
    } catch (e) {
        throw e
    }
}

export {
    getDashBoard
}