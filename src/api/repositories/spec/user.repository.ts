import { db_spec } from '../../../config/db'

const findUserInfo = async (userId: string) => {
    try {
        const result = await db_spec.oneOrNone(
            `
        SET TIME ZONE 'Asia/Seoul';
        SELECT * FROM "user" WHERE id = $1
        `
            , [userId])

        return result
    } catch (e) {
        throw e
    }
}

const getUserList = async (caseId: number, requesterId?: string, isManager?: any): Promise<any> => {
    try {
        let userIdQuery =
            `
        id 
        IN (
            SELECT user_id 
            FROM case_participant 
            WHERE case_id = $1 
            
        )
        `
        if (requesterId) {
            if (!isManager) userIdQuery = `id = '${requesterId}'`
        }

        const result = await db_spec.any(
            `
        SELECT 
            id, name 
        FROM "user" 
        WHERE 
        ${userIdQuery}
        `
            , [caseId])

        return result
    } catch (e) {
        throw e
    }
}

export {
    findUserInfo
    , getUserList
}