import multer from 'multer'
import multerS3 from 'multer-s3'
import AWS from 'aws-sdk'

const s3 = new AWS.S3({
  region: process.env.AWS_REGION
})

const storage = multerS3({
  s3,
  bucket: process.env.AWS_S3_BUCKET,
  contentType: multerS3.AUTO_CONTENT_TYPE,
  metadata: (_req, file, cb) => {
    cb(null, { fieldName: file.fieldname })
  },
  key: (_req, file, cb) => {
    cb(null, `${Date.now()}_${file.originalname}`)
  }
})

export default multer({ storage })
