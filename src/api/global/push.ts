import _ from 'lodash'
import { v4 as uuid4 } from 'uuid'
import moment from 'moment'
import Redis from 'ioredis'

import transporter from '../../config/email'
import logger from '../../config/logger'
import config from '../../config/spec'


const emailSender = (template: any, user: any) => {
  try {
    if (template !== null) {
      let to = ''
      if (typeof user === 'string') to = user
      else to = user.email

      const mailOptions: any = {
        from: config.email.address,
        to: to,
        subject: `${template.subject}`
      }
      if (template.hasOwnProperty('html')) mailOptions.html = template.html
      if (template.hasOwnProperty('attachment')) mailOptions.attachment = template.attachment

      transporter.sendMail(mailOptions, async (err) => {
        if (err) logger.error(err.message)
      })
    } else throw new Error(`NOT FOUND template!`)
  } catch (e) {
    logger.error(e.message)
  }
}

const redisPushSender = async (type: string, caseId: number, content: any, from: string, to?: string, topic: string = 'event') => {
  try {
    // 요청자 있는 경우
    if (!to) to = `#case_${caseId}`

    // event 객체 정의
    const event = {
      eventId: uuid4(),
      timestamp: moment().format(),
      from: from,
      case_id: caseId,
      to: to,
      type: type,
      content: content
    }

    const pub = new Redis()
    pub.publish(topic, JSON.stringify(event))
  } catch (e) {
    logger.error(e.message)
  }
}
export {
  emailSender,
  redisPushSender
}
