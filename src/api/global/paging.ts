import { Request } from 'express'

export const query = (req: Request, query: string) => {
  const limit = req.query.limit as string
  const offset = req.query.offset as string

  if (limit) {
    query += ` LIMIT ${limit}`
  }
  if (offset) {
    query += ` OFFSET ${offset}`
  }

  return query
}
