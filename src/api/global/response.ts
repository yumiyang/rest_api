import { Response } from 'express'
import httpStatus from 'http-status'

const defaultError = (res: Response, msg?: string) => {
  return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
    result: false,
    data: msg
  })
}

const response = (res: Response, data?: object | string) => {
  return res.status(httpStatus.OK).json({
    result: true,
    data: data
  })
}
export { defaultError, response }
