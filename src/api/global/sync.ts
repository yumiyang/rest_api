import mariadb from 'mariadb'
import _ from 'lodash'
import { v4 as uuidv4 } from 'uuid'

import { db } from '../../config/db'
import logger from '../../config/logger'

export default async function () {
  logger.info('Sync Start')

  const pool = mariadb.createPool({
    host: process.env.GW_DATABASE_HOST,
    user: process.env.GW_DATABASE_USER,
    password: process.env.GW_DATABASE_PASS,
    database: process.env.GW_DATABASE_DB
  })

  let conn;

  try {
    conn = await pool.getConnection()

    const rows = await conn.query(`
      SELECT DISTINCT
        v_insa_user.*,
        v_insa_my_job.dept_name AS department,
        v_insa_office.ofc_name AS office
      FROM
        v_insa_my_job
      LEFT JOIN
        v_insa_user
          ON LOWER(v_insa_my_job.emp_no) = LOWER(v_insa_user.emp_no)
      LEFT JOIN
        v_insa_office
          ON v_insa_office.ofc_id = v_insa_user.ofc_id
      WHERE
        v_insa_my_job.bass_dept_yn = 'Y' AND
        v_insa_my_job.dept_id NOT LIKE 'DELETE%'
    `)

    await Promise.all(_.map(rows, async user => {
      try {
        if (user.PROFILE_IMG && user.PROFILE_IMG.substr(0, 20) === 'http://13.124.219.29') {
          user.PROFILE_IMG = 'https://mgw.cncityenergy.com' + user.PROFILE_IMG.split('http://13.124.219.29')[1]
        }

        if (user.PROFILE_IMG === 'null') user.PROFILE_IMG = null

        await db.none(`
          INSERT INTO
            "user"
            ("id", "company", "name", "gwid", "gwno", "email", "photo", "department", "position", "phone", "role")
          VALUES
            ('${uuidv4()}', '${user.CMP_NAME}', '${user.USER_NAME}', '${user.EMP_ID}', '${user.EMP_NO}', '${user.CMP_EMAIL}', '${user.PROFILE_IMG}', '${user.department}', '${user.office}', '${user.CELL_PHONE}', 'user')
          ON CONFLICT ON CONSTRAINT user_gwid_key
            DO UPDATE
              SET ("company", "name", "email", "photo", "department", "position", "phone", "gwno") = (EXCLUDED."company", EXCLUDED."name", EXCLUDED."email", EXCLUDED."photo", EXCLUDED."department", EXCLUDED."position", EXCLUDED."phone", EXCLUDED."gwno")
            ;
        `)
      } catch (error) {
        console.log(error)
        throw error
      }
    }))

    logger.info('DB Inserted')
  } catch (err) {
    console.log(err)
    throw err
  } finally {
    if (conn) return conn.end()
  }
}
