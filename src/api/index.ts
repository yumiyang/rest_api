import 'dotenv/config'
import express, { Request, Response, NextFunction } from 'express'
import createError from 'http-errors'
import cookieParser from 'cookie-parser'
import morgan from 'morgan'
import cors from 'cors'
import swaggerUi from 'swagger-ui-express'

import { response } from './global/response'
import logger from '../config/logger'

import swaggerSpec from '../swagger'
import authRoutes from './routes/auth'
import restRoutes from './routes/rest'
import specRoutes from './routes/spec'
import rtRoutes from './routes/rt'

export default async ({ app }: { app: express.Application }) => {
  app.use(morgan('dev'))
  app.use(express.json())
  app.use(express.urlencoded({ extended: true }))
  app.use(cookieParser())
  app.use(cors())
  app.set('etag', false)

  app.get('/', (_req, res) => {
    response(res)
  })

  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
  app.use('/auth', authRoutes)
  app.use('/rest', restRoutes)
  app.use('/spec', specRoutes)
  app.use('/rt', rtRoutes)

  app.use((_req: Request, _res: Response, next: NextFunction) => {
    next(createError(404))
  })

  app.use((err: any, req: Request, res: Response, _next: NextFunction) => {
    logger.verbose(`query: ${JSON.stringify(req.query)}`)
    logger.verbose(`params: ${JSON.stringify(req.params)}`)
    logger.verbose(`body: ${JSON.stringify(req.body)}`)
    console.log(err)
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    if (!err.status) err.status = 500

    res.status(err.status).json({
      result: false,
      data: err.message
    })
  })

  logger.info('API Initialize Success')
}
