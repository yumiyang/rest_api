import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { response } from '../../global/response'
import * as workGroupRepo from '../../repositories/spec/workgroup.repository'
import * as caseRepo from '../../repositories/spec/case.repository'

const getCaseInfoList = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data: any = {}
        const workgroupId = _.parseInt(req.params.groupId)
        const userId = req.user.userId
        const caseId = req.params.caseId
        const categoryInfo = await workGroupRepo.getCategoryName(workgroupId)
        const caseList = await workGroupRepo.getWorkGroupCaseList(workgroupId, userId, caseId)

        const resultCaseList: any[] = []

        // case 별 참여자 id 리스트
        for (const caseObj of caseList) {
            let participantObj: any = {}
            const participantIdList = await caseRepo.getCaseMemberIdList(caseObj.id)
            participantObj.participantIdList = _.join(participantIdList, ',')

            resultCaseList.push(_.assign(caseObj, participantObj))
        }

        data.categoryInfo = categoryInfo
        data.caseList = resultCaseList

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const getAllUserList = async (_req: Request, res: Response, next: NextFunction) => {
    try {
        const data = await workGroupRepo.getaAllUserList()

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const getAllWorkgroupList = async (req: any, res: Response, next: NextFunction) => {
    try {
        const data = await workGroupRepo.getAllWorkgoupList(req.user.userId)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

export {
    getCaseInfoList
    , getAllUserList
    , getAllWorkgroupList
}