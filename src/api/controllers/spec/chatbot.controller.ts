import { Request, Response, NextFunction } from 'express'
import { response } from '../../global/response'
import * as chatBotRepo from '../../repositories/spec/chatbot.repository'

const getChatBotAnswerList = async (req: any, res: Response, next: NextFunction) => {
    try {
        const data: any = {}
        data.answerList = await chatBotRepo.getChatBotAnswerList()
        data.email = req.user.email

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const updateChatBotAnswer = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data: any = { result: true }
        const params: any = {}
        const values = req.body

        if (values.hasOwnProperty('label')) params.label = values.label
        if (values.hasOwnProperty('text')) params.text = values.text

        const result = await chatBotRepo.updateChatBotAnswer(params, req.params.type)
        if (!result) data.result = false

        response(res, data)
    } catch (e) {
        next(e)
    }
}

export {
    getChatBotAnswerList
    , updateChatBotAnswer
}