import _ from 'lodash'
import { Request, Response, NextFunction } from 'express'
import AWS from 'aws-sdk'
import { response } from '../../global/response'
import * as caseRepo from '../../repositories/spec/case.repository'
import config from '../../../config/spec'
import {
    redisPushSender
} from '../../global/push'

const s3 = new AWS.S3({
    region: process.env.AWS_REGION
})

const getCaseMemberList = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const caseId: number = _.parseInt(req.params.caseId)
        const data = await caseRepo.getCaseMemberList(caseId)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const addCaseMember = async (req: any, res: Response, next: NextFunction) => {
    try {
        let data: any = {}
        const caseId: number = _.parseInt(req.params.caseId)
        const memberList = []
        const userIdList = []

        for (const user of _.split(req.body.userList, ',')) {
            let member: any = {}
            member.case_id = caseId
            member.user_id = user

            memberList.push(member)
            userIdList.push(user)
        }

        const result = await caseRepo.addCaseMember(memberList)

        // redis push
        _.forEach(result, async (res) => {
            await redisPushSender(config.noti.type.invitaionToParticipant, caseId, '', req.user.userId, res.userId)
        })

        data.success = result.length

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const deleteCaseMember = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data: any = { result: true }
        const caseId: number = _.parseInt(req.params.caseId)
        const result = await caseRepo.deleteCaseMember(caseId, req.params.userId)

        if (result.length === 0) data.result = false

        await redisPushSender('case.participant.leave', caseId, 'leave', req.user.userId, req.params.userId)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const getFileList = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const caseId: number = _.parseInt(req.params.caseId)
        const data = await caseRepo.getFileList(caseId, req.params.type)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const getPinList = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const caseId: number = _.parseInt(req.params.caseId)
        const data = await caseRepo.getPinList(caseId)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const downloadFile = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const fileId = req.params.fileId
        const fileInfo = await caseRepo.getFileInfo(fileId)
        s3.getObject({ Bucket: process.env.AWS_S3_BUCKET, Key: fileInfo.key })
            .createReadStream()
            .pipe(res)
    } catch (e) {
        next(e)
    }
}

const addPin = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data: any = { result: true }
        const caseId: number = _.parseInt(req.params.caseId)
        const result = await caseRepo.addPin(caseId, req.params.messageId)

        if (!result) data.result = false
        await redisPushSender('case.bookmark.add', caseId, '', 'system', caseId.toString(), 'system')
        response(res, data)
    } catch (e) {
        next(e)
    }
}

const requestCase = async (req: any, res: Response, next: NextFunction) => {
    try {
        const type: string = req.query.type
        const caseId: number | string = _.parseInt(req.params.caseId)
        const requesterId = req.user.userId
        let notiType: string = ''
        let data = { result: true }

        if (type !== 'move') {
            const isManager = await caseRepo.isManager(caseId, requesterId)
            if (type === 'end') {
                if (!isManager) notiType = config.noti.type.requestOfCaseEnd
                else notiType = config.noti.type.caseEnd
            } else if (type === 'delete') {
                if (!isManager) notiType = config.noti.type.requestOfCaseDelete
                else notiType = config.noti.type.caseDelete
            }
            // 케이스 종료 or 삭제 (only 케이스 처리자)
            if (await caseRepo.updateCaseStatus(caseId, notiType, requesterId) === null) data.result = false
        } else {
            notiType = config.noti.type.caseMove
            // 케이스 다른 워크그룹으로 이동
            const workgroupId: number = _.parseInt(req.body.workgroupId)
            if (await caseRepo.updateCaseWorkgroupId(caseId, workgroupId) === null) data.result = false
        }

        await redisPushSender(notiType, caseId, { beforeWorkgroupId: req.body.workgroupId }, req.user.userId)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const deletePin = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data: any = { result: true }
        const caseId: number = _.parseInt(req.params.caseId)
        const result = await caseRepo.deletePin(caseId, req.params.messageId)

        if (!result) data.result = false
        await redisPushSender('case.bookmark.delete', caseId, '', 'system', caseId.toString(), 'system')
        response(res, data)
    } catch (e) {
        next(e)
    }
}

const getFileHistoryList = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const caseId: number = _.parseInt(req.params.caseId)
        const data = await caseRepo.getFileHistoryList(caseId, req.params.productId, req.params.version)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const getNotiHistoryList = async (req: any, res: Response, next: NextFunction) => {
    try {
        const data = await caseRepo.getNotiHistoryList(req.user.userId)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const updateNotiChecked = async (req: Request, res: Response, next: NextFunction) => {
    try {
        let data: any = { result: true }
        let eventIdList: string[] = []

        _.forEach(req.body.eventIdList, (eventIdObj) => {
            eventIdList.push(eventIdObj.id)
        })
        const result = await caseRepo.updateNotiChecked(eventIdList)

        if (result.length === 0) data.result = false

        response(res, data)
    } catch (e) {
        next(e)
    }
}

const sendProductFile = async (req: any, res: Response, next: NextFunction) => {
    try {
        const caseId: number = _.parseInt(req.params.caseId)
        const productInfo = await caseRepo.getFileInfo(req.params.fileId)

        if (typeof req.body.recipientList === 'string') req.body.recipientList = [req.body.recipientList]
        _.forEach(req.body.recipientList, async (recipient) => {
            await redisPushSender(config.noti.type.sendProductFile, caseId, productInfo, req.user.userId, recipient)
        })

        response(res)
    } catch (e) {
        next(e)
    }
}

export {
    getCaseMemberList
    , addCaseMember
    , deleteCaseMember
    , getFileList
    , getPinList
    , downloadFile
    , addPin
    , requestCase
    , deletePin
    , getFileHistoryList
    , getNotiHistoryList
    , updateNotiChecked
    , sendProductFile
}