import { Request, Response, NextFunction } from 'express'
import { response } from '../../global/response'
import * as dashBoardRepo from '../../repositories/spec/dashBoard.repository'

const getDashBoard = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const data = await dashBoardRepo.getDashBoard(req.user.userId, req.params.status)

        response(res, data)
    } catch (e) {
        next(e)
    }
}

export {
    getDashBoard
}