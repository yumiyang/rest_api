import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

export const unreadAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        *
      FROM
        "notification"
      WHERE
        "type" = 'notification' AND
        "to" = '${req.user.userId}' AND 
        (
          timestamp < NOW() - INTERVAL '7 days' OR
          checked IS NULL
        )
      ORDER BY
        timestamp DESC
    `

    sql = paging.query(req, sql)

    const messages = await db.manyOrNone(sql)
    response(res, messages)
  } catch (e) {
    next(e)
  }
}

export const all = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        *
      FROM
        "notification"
      WHERE
        "type" = 'notification' AND
        "to" = '${req.user.userId}'
      ORDER BY
        timestamp DESC
    `

    sql = paging.query(req, sql)

    const messages = await db.manyOrNone(sql)
    response(res, messages)
  } catch (e) {
    next(e)
  }
}

export const unreadCheck = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      UPDATE
        "notification"
      SET
        checked = NOW()
      WHERE
        "id" IN (${req.body.notifications.split(',').map((n: string) => `'${_.trim(n)}'`)})
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}
