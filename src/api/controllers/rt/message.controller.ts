import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

export const unreadAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        *
      FROM
        "notification"
      WHERE
        "type" = 'message' AND
        "case_id" = '#case_${req.body.case_id}' AND
        "to" = '${req.user.userId}'
      ORDER BY
        timestamp ASC
    `

    sql = paging.query(req, sql)

    const messages = await db.manyOrNone(sql)
    response(res, messages)
  } catch (e) {
    next(e)
  }
}

export const unreadCount = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
    SELECT
      *
    FROM
      "notification"
    WHERE
      "type" = 'message' AND
      "to" = '${req.user.userId}'
  `
    const messages = await db.manyOrNone(sql)

    const result = _(messages)
      .groupBy('case_id')
      .map((r, k) => {
        return {
          case_id: _.toInteger(k.split('_')[1]),
          count: r.length
        }
      })

    response(res, result)
  } catch (e) {
    next(e)
  }
}

export const unreadCheck = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "notification"
      WHERE
        "type" = 'message' AND
        "case_id" = '#case_${req.body.case_id}' AND
        "to" = '${req.user.userId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}
