import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'
import Redis from 'ioredis'
import moment from 'moment'
import { v4 as uuidv4 } from 'uuid'

import { db } from '../../../config/db'
import { response } from '../../global/response'


export const voteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const caseId = req.body.case_id
    const agreement = req.body.agreement

    await db.none(`
      UPDATE 
        "vote"
      SET
        "agreement" = '${agreement}'
      WHERE
        "case_id" = '${caseId}' AND
        "user_id" = '${req.user.userId}'
    `)

    if (agreement === 'true' || agreement === true) {
      await checkVoting(caseId)
    } else {
      await cancelVote(req.user.userId, caseId)
    }


    response(res)
  } catch (e) {
    next(e)
  }
}

export const checkMe = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const vote = await db.oneOrNone(`
      SELECT
        *
      FROM
        "vote"
      WHERE
        "case_id" = '${req.body.case_id}' AND
        "user_id" = '${req.user.userId}' AND
        "agreement" IS NOT NULL
      ORDER BY
        timestamp
          DESC LIMIT 1
    `)

    if (_.isEmpty(vote)) {
      response(res, { vote: false })
    } else {
      response(res, { vote: true })
    }
  } catch (e) {
    next(e)
  }
}

async function checkVoting(caseId: string) {
  const votes = await db.manyOrNone(`
    SELECT
      *
    FROM
      "vote"
    WHERE
      "case_id" = '${caseId}' AND
      "agreement" IS NULL
  `)

  if (_.isEmpty(votes)) {
    await db.none(`
      UPDATE
        "case"
      SET
        "status" = '종료',
        "close_time" = NOW(),
        "progress" = 100
      WHERE
        "id" = '${caseId}'
    `)

    const pub = new Redis()
    await pub.publish('event', JSON.stringify({
      type: 'case.end',
      case_id: caseId,
      from: 'system',
      timestamp: moment().format()
    }))

    pub.disconnect()
  }
}

async function cancelVote(userId: string, caseId: string) {
  await db.none(`
      UPDATE
        "case"
      SET
        "status" = '진행중',
        "close_request_time" = NULL,
        "close_issuer" = NULL,
        "close_time" = NULL
      WHERE
        "id" = '${caseId}'
    `)

  await db.none(`
    DELETE FROM
      "vote"
    WHERE
      "case_id" = '${caseId}'
  `)

  const pub = new Redis()
  await pub.publish('event', JSON.stringify({
    id: uuidv4(),
    from: userId,
    type: 'case.end.cancel',
    case_id: caseId,
    timestamp: moment().format()
  }))

  pub.disconnect()
}