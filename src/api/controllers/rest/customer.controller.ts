import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'

const customerColumns = new pgp.helpers.ColumnSet([
  { name: 'name', skip: helpers.skip },
  { name: 'description', def: null, skip: helpers.skip }
])

const readAll = async (_req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        *
      FROM
        customer
    `

    const categories = await db.manyOrNone(sql)

    response(res, categories)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.customerId

    const categories = await db.one(`
      SELECT
        *
      FROM
        customer
      WHERE
        id = '${id}'
    `)

    response(res, categories)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, customerColumns, 'customer') + ' RETURNING *'
    const result = await db.one(query)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, customerColumns, 'customer') +
      ` WHERE id = ${req.params.customerId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "customer"
      WHERE
        id = '${req.params.customerId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
