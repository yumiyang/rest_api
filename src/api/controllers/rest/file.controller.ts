import { v4 as uuidv4 } from 'uuid'
import { Request, Response, NextFunction } from 'express'
import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'

import moment from 'moment'
import Redis from 'ioredis'
import AWS from 'aws-sdk'

const s3 = new AWS.S3({
  region: process.env.AWS_REGION
})

const redis = new Redis()

const fileColumns = new pgp.helpers.ColumnSet([
  '?id',
  { name: 'location', def: null, skip: helpers.skip },
  { name: 'mimetype', def: null, skip: helpers.skip },
  { name: 'originalname', def: null, skip: helpers.skip },
  { name: 'key', def: null, skip: helpers.skip },
  { name: 'size', def: null, skip: helpers.skip }
])

const caseFileColumns = new pgp.helpers.ColumnSet([
  'case_id',
  'file_id',
  { name: 'filetype', def: null, skip: helpers.skip },
  { name: 'product_id', def: null, skip: helpers.skip },
  { name: 'version', def: 1, skip: helpers.skip },
  { name: 'created_by', def: null, skip: helpers.skip }
])

export const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        file.*
      FROM
        file
    `

    if (req.params.caseId) {
      sql += `
        LEFT JOIN
          case_file
            ON
              case_file.file_id = file.id
        LEFT JOIN
          "case"
            ON
              case_file.case_id = "case".id
        WHERE
          "case".id = '${req.params.caseId}'
      `
    }

    const files = await db.manyOrNone(sql)

    response(res, files)
  } catch (e) {
    next(e)
  }
}

export const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const file = await db.one(`
      SELECT
        *
      FROM
        file
      FULL OUTER JOIN
        case_file
          ON
            case_file.file_id = file.id
      WHERE
        id = '${req.params.fileId}'
    `)

    response(res, file)
  } catch (e) {
    next(e)
  }
}

export const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.file) {
      throw new Error('File이 없습니다')
    }
    req.body.id = uuidv4()

    const fileData = Object.assign(req.body, req.file)

    const query = pgp.helpers.insert(fileData, fileColumns, 'file') + ' RETURNING *'

    const result = await db.one(query)

    if (req.body.case_id) {
      fileData.file_id = result.id
      fileData.product_id = uuidv4()
      fileData.created_by = req.user.userId
      fileData.filetype = req.body.filetype || 'attachment'

      const relationQuery = pgp.helpers.insert(fileData, caseFileColumns, 'case_file')
      await db.none(relationQuery)

      await redis.publish('system',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.attach.product',
          timestamp: moment().format(),
          to: `#case_${req.body.case_id}`,
          case_id: req.body.case_id,
          from: req.user.userId,
          content: fileData
        })
      )
    }

    response(res, result)
  } catch (e) {
    next(e)
  }
}

export const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {

    console.log(req.body)
    if (!req.file) {
      throw new Error('file이 없습니다')
    }

    if (!req.body.case_id) {
      throw new Error('case_id 가 없습니다')
    }

    req.body.id = uuidv4()

    const fileData = Object.assign(req.body, req.file)

    const query = pgp.helpers.insert(fileData, fileColumns, 'file') + ' RETURNING *'

    const result = await db.one(query)

    const lastVersion = await db.one(`
      SELECT
        version
      FROM
        case_file
      WHERE
        product_id = '${req.body.product_id}'
      ORDER BY
        version
          DESC
      LIMIT 1
    `)

    fileData.file_id = result.id
    fileData.created_by = req.user.userId
    fileData.version = Number(lastVersion.version) + 1
    fileData.filetype = 'product'

    const relationQuery = pgp.helpers.insert(fileData, caseFileColumns, 'case_file')
    await db.none(relationQuery)

    await redis.publish('system',
      JSON.stringify({
        eventId: uuidv4(),
        type: 'case.update.product',
        timestamp: moment().format(),
        to: `#case_${req.body.case_id}`,
        case_id: req.body.case_id,
        from: req.user.userId,
        content: fileData
      })
    )

    response(res, result)
  } catch (e) {
    next(e)
  }
}

export const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.fileId

    let fileInfo = await db.one(`
      SELECT
        *
      FROM
        file
      WHERE
        id = '${id}'
    `)

    s3.deleteObject(
      {
        Bucket: process.env.AWS_S3_BUCKET,
        Key: fileInfo.key
      },
      async (err, data) => {
        if (err) return next(err)

        await db.none(`
          DELETE FROM
            file
          WHERE
            id = '${id}'
        `)

        const caseId = req.query.caseId || '0'

        await redis.publish('system',
          JSON.stringify({
            eventId: uuidv4(),
            type: 'case.delete.product',
            timestamp: moment().format(),
            to: `#case_${caseId}`,
            case_id: caseId,
            from: req.user.userId,
            content: fileInfo
          })
        )

        response(res, data)
      }
    )
  } catch (e) {
    next(e)
  }
}

export const downloadOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const fileInfo = await db.one(`
    SELECT
      *
    FROM
      file
    WHERE
      id = '${req.params.fileId}'
  `)

    const fileObjectStream = s3.getObject({ Bucket: process.env.AWS_S3_BUCKET, Key: fileInfo.key })
      .createReadStream()

    res.set('Content-Type', 'application/octet-stream')
    res.set(
      'Content-Disposition',
      'attachment; filename="' + fileInfo.originalname + '"'
    )
    fileObjectStream.pipe(res)
  } catch (e) {
    next(e)
  }
}