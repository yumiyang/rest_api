import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'
import { v4 as uuidv4 } from 'uuid'
import moment from 'moment'
import Redis from 'ioredis'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

const redis = new Redis()

const caseColumns = new pgp.helpers.ColumnSet([
  { name: 'workgroup_id', def: null, skip: helpers.skip },
  { name: 'name', skip: helpers.skip },
  { name: 'status', def: null, skip: helpers.skip },
  { name: 'start_date', def: null, init: helpers.dateWrapper, skip: helpers.skip },
  { name: 'end_date', def: null, init: helpers.dateWrapper, skip: helpers.skip },
  { name: 'target_date', def: null, init: helpers.dateWrapper, skip: helpers.skip },
  { name: 'request_date', def: null, init: helpers.dateWrapper, skip: helpers.skip },
  { name: 'progress', def: null, skip: helpers.skip },
  { name: 'issuer', def: null, skip: helpers.skip },
  { name: 'manager', def: null, skip: helpers.skip },
  { name: 'close_request_time', def: null, skip: helpers.skip },
  { name: 'close_time', def: null, skip: helpers.skip },
  { name: 'close_issuer', def: null, skip: helpers.skip }
])

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
        SELECT
          *
        FROM
          "case"`

    if (req.params.workgroupId) {
      sql += `
        WHERE
          workgroup.id = ${req.params.workgroupId}
        ORDER BY
          id ASC
      `
    }

    sql = paging.query(req, sql)

    const cases = await db.manyOrNone(sql)
    response(res, cases)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.caseId

    const cases = await db.one(`
      SELECT
        *,
        ARRAY(
          SELECT
            user_id
          FROM
            "case_participant"
          WHERE
            case_id = '${id}'
        ) AS "case_participant"
      FROM
        "case"
      WHERE
        id = '${id}'
    `)

    response(res, cases)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, caseColumns, 'case') + ' RETURNING *'
    const result = await db.one(query)

    if (req.body.participant) {
      await db.none(`
        INSERT INTO
          "case_participant"
          (case_id, user_id)
          VALUES
          ${_.map(req.body.participant.split(','), part => `('${result.id}', '${part.trim()}')`)}
      `)
    }
    await redis.publish('event',
      JSON.stringify({
        eventId: uuidv4(),
        type: 'case.create',
        timestamp: moment().format(),
        to: result.manager,
        case_id: result.id,
        from: req.user.userId,
        content: result
      })
    )

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const caseInfo = await db.one(`
      SELECT
        *,
        ARRAY(
          SELECT
            user_id
          FROM
            "case_participant"
          WHERE
            case_id = '${req.params.caseId}'
        ) AS "case_participant"
      FROM
        "case"
      WHERE
        id = '${req.params.caseId}'
    `)

    if (req.body.participant) {
      const caseParticipantByDB = caseInfo.case_participant.sort()
      const caseParticipantByRequest = req.body.participant.split(',').map((i: string) => i.trim()).sort()

      if (!_.isEqual(caseParticipantByDB, caseParticipantByRequest)) {
        await Promise.all(_.difference(caseParticipantByDB, caseParticipantByRequest).map(async user => {
          await db.none(`
            DELETE FROM
              "case_participant"
            WHERE
              "case_id" = '${req.params.caseId}' AND
              "user_id" = '${user}'
          `)
          await redis.publish('event',
            JSON.stringify({
              eventId: uuidv4(),
              type: 'case.participant.leave',
              timestamp: moment().format(),
              to: user,
              case_id: req.params.caseId,
              from: req.user.userId,
              content: req.body
            })
          )
        }))

        if (_.difference(caseParticipantByRequest, caseParticipantByDB).length > 0) {
          await Promise.all(_.difference(caseParticipantByRequest, caseParticipantByDB).map(async user => {
            await db.none(`
              INSERT INTO
                "case_participant"
                (case_id, user_id)
                VALUES
                ('${req.params.caseId}', '${user}')
            `)
            await redis.publish('event',
              JSON.stringify({
                eventId: uuidv4(),
                type: 'case.participant.invite',
                timestamp: moment().format(),
                to: user,
                case_id: req.params.caseId,
                from: req.user.userId,
                content: req.body
              })
            )
          }))
        }
      }
    }

    if (req.body.progress && caseInfo.progress !== _.toInteger(req.body.progress)) {
      await redis.publish('event',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.update.progress',
          timestamp: moment().format(),
          to: `#case_${req.params.caseId}`,
          case_id: req.params.caseId,
          from: req.user.userId,
          content: req.body
        })
      )
    }

    if (req.body.manager && caseInfo.manager !== req.body.manager) {
      await redis.publish('event',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.manager.change',
          timestamp: moment().format(),
          to: req.body.manager,
          case_id: req.params.caseId,
          from: req.user.userId,
          content: req.body
        })
      )
    }

    if (req.body.status === '종료 요청') {
      await redis.publish('event',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.end.request',
          timestamp: moment().format(),
          to: `#case_${req.params.caseId}`,
          case_id: req.params.caseId,
          from: req.user.userId,
          content: req.body
        })
      )

      req.body.close_issuer = req.user.userId
      req.body.close_request_time = moment().format()
    }

    if (req.body.status === '삭제 요청') {
      await redis.publish('event',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.delete.request',
          timestamp: moment().format(),
          to: `#case_${req.params.caseId}`,
          case_id: req.params.caseId,
          from: req.user.userId,
          content: req.body
        })
      )

      req.body.delete_issuer = req.user.userId
      req.body.delete_request_time = moment().format()
    }

    if (req.body.status === '삭제') {
      await redis.publish('event',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.delete',
          timestamp: moment().format(),
          to: `#case_${req.params.caseId}`,
          case_id: req.params.caseId,
          from: req.user.userId,
          content: req.body
        })
      )
    }

    if (req.body.status === '진행중') {
      await redis.publish('event',
        JSON.stringify({
          eventId: uuidv4(),
          type: 'case.restore',
          timestamp: moment().format(),
          to: `#case_${req.params.caseId}`,
          case_id: req.params.caseId,
          from: req.user.userId,
          content: req.body
        })
      )
    }

    const result =
      pgp.helpers.update(req.body, caseColumns, 'case') + ` WHERE id = ${req.params.caseId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "case"
      WHERE
        id = '${req.params.caseId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
