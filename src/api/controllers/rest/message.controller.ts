import { Request, Response, NextFunction } from 'express'
import createError from 'http-errors'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

const messageColumns = new pgp.helpers.ColumnSet([
  { name: 'content', def: null, skip: helpers.skip }
])

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const limit = req.query.limit as string || 0
    console.log(limit)

    const message = await db.many(`
      (
        SELECT
          *
        FROM
          "message"
        WHERE
          timestamp > (SELECT timestamp FROM "message" WHERE id = '${req.params.messageId}')
        ORDER BY timestamp ASC
        LIMIT ${limit}
      )
      
      UNION
      
      SELECT
        *
      FROM
        "message"
      WHERE
        id = '5d3d877b-10f0-4196-881c-a01e4cb956cd'
      
      UNION
      
      (
        SELECT
          *
        FROM
          "message"
        WHERE
          timestamp < (SELECT timestamp FROM "message" WHERE id = '${req.params.messageId}')
        ORDER BY timestamp DESC
        LIMIT ${limit}
      )
      
      ORDER BY timestamp
    `)

    response(res, message)
  } catch (e) {
    next(e)
  }
}

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    if (!req.params.caseId) return next(createError(400, 'Case URL을 통해 요청해주세요'))

    if (!req.query.order) req.query.order = 'DESC'

    let sql = `
      SELECT
        "message".*,
        CASE WHEN
          bookmark.message_id IS NOT NULL
            THEN
              true
            ELSE
              false
        END AS bookmark
      FROM
        "message"
      LEFT JOIN
        "bookmark"
          ON
            "message".id = bookmark.message_id
      WHERE
        "to" = '#case_${req.params.caseId}'
      ORDER BY
        timestamp
          ${req.query.order}
    `

    sql = paging.query(req, sql)

    const messages = await db.manyOrNone(sql)
    response(res, messages)
  } catch (e) {
    next(e)
  }
}

const searchOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const content = req.query.content
    const caseId = req.query.case_id

    if (content === undefined) return next(createError(400, 'Content가 필요합니다'))
    if (typeof content === 'string' && content.length < 2) return next(createError(400, 'Content는 2글자 이상 입력해주세요'))

    const result = await db.any(`
      SELECT
        *
      FROM
        message
      WHERE
        "to" = '#case_${caseId}' AND
        "content" LIKE '%${content}%'
      ORDER BY
        timestamp ASC
    `)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, messageColumns, 'message') +
      ` WHERE id = ${req.params.messageId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "message"
      WHERE
        id = '${req.params.messageId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readOne, readAll, searchOne, updateOne, deleteOne }
