import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'

const categoryColumns = new pgp.helpers.ColumnSet([
  { name: 'name', skip: helpers.skip },
  { name: 'description', def: null, skip: helpers.skip },
  { name: 'section_id', def: null, skip: helpers.skip }
])

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        customer.id AS customer_id,
        customer.name AS customer_name,
        section.id AS section_id,
        section.name AS section_name,
        category.id,
        category.name,
        category.description
      FROM
        category
      FULL OUTER JOIN
        section
          ON
            category.section_id = section.id
      FULL OUTER JOIN
        customer
          ON
            section.customer_id = customer.id
    `

    if (req.params.sectionId) {
      sql += `
        WHERE
          section.id = ${req.params.sectionId}
      `
    }

    sql += ` ORDER BY category.id`

    const categories = await db.manyOrNone(sql)

    response(res, categories)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.categoryId

    const category = await db.one(`
      SELECT
        customer.id AS customer_id,
        customer.name AS customer_name,
        section.id AS section_id,
        section.name AS section_name,
        category.id,
        category.name,
        category.description
      FROM
        section
      FULL OUTER JOIN
        customer
          ON
            section.customer_id = customer.id
      FULL OUTER JOIN
        category
          ON
            category.section_id = section.id
      WHERE
        category.id = '${id}'
    `)

    response(res, category)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, categoryColumns, 'category') + ' RETURNING *'
    const result = await db.one(query)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, categoryColumns, 'category') +
      ` WHERE id = ${req.params.categoryId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "category"
      WHERE
        id = '${req.params.categoryId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
