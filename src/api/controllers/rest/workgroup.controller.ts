import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

const workgroupColumns = new pgp.helpers.ColumnSet([
  { name: 'category_id', skip: helpers.skip },
  { name: 'name', skip: helpers.skip },
  { name: 'manager', def: null, skip: helpers.skip },
  {
    name: 'submanager', def: null, skip: c => !c.exists, init: c => {
      if (c.value === 'null') {
        return null
      } else return c.value
    }
  },
  { name: 'description', def: null, skip: helpers.skip }
])

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const userInfo = await db.one(`
      SELECT
        *
      FROM
        "user"
      WHERE
        id = '${req.user.userId}'
    `)

    let sql: string

    if (userInfo.role === 'administrator' || !req.query.me) {
      sql = `
        SELECT
          "category".name AS category_name,
          "workgroup".*
        FROM
          "workgroup"
        FULL OUTER JOIN
          "category"
            ON
              "category".id = "workgroup".category_id
      `

      if (req.params.categoryId) {
        sql += `
          WHERE
            category.id = ${req.params.categoryId}
        `
      }
    } else {
      sql = `
    SELECT
      "category".name AS category_name,
      "workgroup".*
    FROM
      "workgroup"
    FULL OUTER JOIN
      "category"
        ON
          "category".id = "workgroup".category_id
    WHERE
      "workgroup".id IN (
        SELECT DISTINCT
          workgroup_id
        FROM
          case_participant
        LEFT JOIN
          "case"
            ON
              "case".id = case_participant.case_id
        WHERE
          user_id = '${req.user.userId}'
          
        UNION
          
        SELECT DISTINCT
          workgroup_id
        FROM
          "case"
        WHERE
          manager = '${req.user.userId}' OR
          issuer = '${req.user.userId}'
          
        UNION

        SELECT DISTINCT
          workgroup.id
        FROM
          workgroup
        WHERE
          manager = '${req.user.userId}' OR
          submanager LIKE '%${req.user.userId}%'
      )
  `

      if (req.params.categoryId) {
        sql += `
          AND
            category.id = ${req.params.categoryId}
        `
      }
    }

    sql += ` ORDER BY workgroup.id ASC `

    sql = paging.query(req, sql)

    const workgroup = await db.manyOrNone(sql)
    response(res, workgroup)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.workgroupId

    const workgroup = await db.one(`
      SELECT
        section.id AS section_id,
        section.name AS section_name,
        category.name AS category_name,
        workgroup.*
      FROM
        "workgroup"
      LEFT JOIN
        category
          ON
            category.id = workgroup.category_id
      LEFT JOIN
            section
              ON
                section.id = category.section_id
          
      WHERE
        workgroup.id = '${id}'
    `)

    response(res, workgroup)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, workgroupColumns, 'workgroup') + ' RETURNING *'
    const result = await db.query(query)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, workgroupColumns, 'workgroup') +
      ` WHERE id = ${req.params.workgroupId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "workgroup"
      WHERE
        id = '${req.params.workgroupId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
