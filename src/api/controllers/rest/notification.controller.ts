import { Request, Response, NextFunction } from 'express'

import { db } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

export const readByCase = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        *
      FROM
        "notification"
      WHERE
        "case_id" = '#case_${req.body.case_id}' AND
        "to" = '${req.user.userId}'
    `

    sql = paging.query(req, sql)

    const messages = await db.manyOrNone(sql)
    response(res, messages)
  } catch (e) {
    next(e)
  }
}

export const check = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "notification"
      WHERE
        "case_id" = '#case_${req.body.case_id}' AND
        "to" = '${req.user.userId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}
