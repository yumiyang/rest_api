import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

const userColumns = new pgp.helpers.ColumnSet([
  { name: 'email', skip: helpers.skip },
  { name: 'name', skip: helpers.skip },
  { name: 'photo', def: null, skip: helpers.skip },
  { name: 'phone', def: null, skip: helpers.skip },
  { name: 'company', def: null, skip: helpers.skip },
  { name: 'position', def: null, skip: helpers.skip },
  { name: 'department', def: null, skip: helpers.skip },
  { name: 'gwid', def: null, skip: helpers.skip },
  { name: 'gwno', def: null, skip: helpers.skip },
  { name: 'role', def: null, skip: helpers.skip }
])

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        id, gwid, gwno, email, name, photo, phone, company, position, department, role, last_login, is_groupware
      FROM
        "user"
    `

    sql = paging.query(req, sql)

    const user = await db.manyOrNone(sql)
    response(res, user)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.userId

    const user = await db.one(`
      SELECT
        id, gwid, gwno, email, name, photo, phone, company, position, department, role, last_login, is_groupware
      FROM
        "user"
      WHERE
        id = '${id}'
    `)

    response(res, user)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, userColumns, 'user') + ' RETURNING *'
    const result = await db.query(query)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, userColumns, 'user') +
      ` WHERE id = '${req.params.userId}'`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "user"
      WHERE
        id = '${req.params.userId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
