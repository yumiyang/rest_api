import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'

const sectionColumns = new pgp.helpers.ColumnSet([
  { name: 'name', skip: helpers.skip },
  { name: 'description', def: null, skip: helpers.skip },
  { name: 'customer_id', def: null, skip: helpers.skip }
])

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        customer.id AS customer_id,
        customer.name AS customer_name,
        section.id,
        section.name,
        section.description
      FROM
        section
      FULL OUTER JOIN
        customer
          ON
            section.customer_id = customer.id
    `

    if (req.params.customerId) {
      sql += `
        WHERE
          customer.id = ${req.params.customerId}
      `
    }

    sql += ` ORDER BY section.id`

    const categories = await db.manyOrNone(sql)

    response(res, categories)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.sectionId

    const categories = await db.one(`
      SELECT
        customer.id AS customer_id,
        customer.name AS customer_name,
        section.id,
        section.name,
        section.description
      FROM
        section
      FULL OUTER JOIN
        customer
          ON
            section.customer_id = customer.id
      WHERE
        section.id = '${id}'
    `)

    response(res, categories)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, sectionColumns, 'section') + ' RETURNING *'
    const result = await db.one(query)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, sectionColumns, 'section') +
      ` WHERE id = ${req.params.sectionId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "section"
      WHERE
        id = '${req.params.sectionId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
