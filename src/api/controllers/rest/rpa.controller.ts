import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { db, pgp, helpers } from '../../../config/db'
import { response } from '../../global/response'
import * as paging from '../../global/paging'

const rpaColumns = new pgp.helpers.ColumnSet([
  { name: 'task_category', def: null, skip: helpers.skip },
  { name: 'source', def: null, skip: helpers.skip },
  { name: 'message', def: null, skip: helpers.skip },
  { name: 'timestamp', def: null, init: helpers.dateWrapper, skip: helpers.skip }
])

const readAll = async (req: Request, res: Response, next: NextFunction) => {
  try {
    let sql = `
      SELECT
        *
      FROM
        rpa_monitoring
    `

    if (req.query.task_category) sql += `
      WHERE
        task_category LIKE '${req.query.task_category}%'
      ORDER BY
        timestamp DESC LIMIT 1
    `

    sql = paging.query(req, sql)

    const result = await db.manyOrNone(sql)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const readOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const id = req.params.rpaId

    const result = await db.one(`
      SELECT
        *
      FROM
        rpa_monitoring
      WHERE
        id = '${id}'
    `)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const createOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const query = pgp.helpers.insert(req.body, rpaColumns, 'rpa_monitoring') + ' RETURNING *'
    const result = await db.one(query)

    response(res, result)
  } catch (e) {
    next(e)
  }
}

const updateOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result =
      pgp.helpers.update(req.body, rpaColumns, 'rpa_monitoring') +
      ` WHERE id = ${req.params.rpaId}`

    await db.none(result)

    response(res)
  } catch (e) {
    next(e)
  }
}

const deleteOne = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await db.none(`
      DELETE FROM
        "rpa_monitoring"
      WHERE
        id = '${req.params.rpaId}'
    `)

    response(res)
  } catch (e) {
    next(e)
  }
}

export { readAll, readOne, createOne, updateOne, deleteOne }
