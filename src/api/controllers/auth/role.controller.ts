import { Request, Response, NextFunction } from 'express'
import _ from 'lodash'

import { response } from '../../global/response'
import { db } from '../../../config/db'

export const readAll = async (_req: Request, res: Response, next: NextFunction) => {
	try {
		const auth = await db.many(`
			SELECT
				*
			FROM
				"authorization"
		`)

		const result = _(auth)
			.groupBy('name')
			.mapValues(row =>
				_(row)
					.mapKeys(r => r.role)
					.mapValues(r => r.permit)
					.value()
			)
			.value()

		response(res, result)
	} catch (e) {
		next(e)
	}
}

export const updateAll = async (req: Request, res: Response, next: NextFunction) => {
	try {
		await db.tx(t => {
			const queries = _.map(req.body, (row, key) => {
				_.map(row, (r, k) => {
					return t.none(`UPDATE "authorization" SET permit = ${r} WHERE name = '${key}' AND "role" = '${k}';`)
				})
			})
			return t.batch(queries)
		})

		response(res)
	} catch (e) {
		next(e)
	}
}

export const readOne = async (_req: Request, res: Response, next: NextFunction) => {
	try {
		const auth = await db.many(`
    SELECT
      *
    FROM
      authorization
  `)

		const result = _(auth)
			.groupBy('role_id')
			.mapValues(row =>
				_(row)
					.mapKeys(r => r.name)
					.mapValues(r => r.permit)
					.value()
			)
			.value()

		response(res, result)
	} catch (e) {
		next(e)
	}
}

export const permissionCheck = async (req: Request, res: Response, next: NextFunction) => {
	try {
		let role = ['guest']

		const baseRole = await db.one(`
			SELECT
				"role"
			FROM
				"user"
			WHERE
				id = '${req.user.userId}'
		`)

		if (req.body.workgroup_id) {
			const workgroupInfo = await db.one(`
				SELECT
					*
				FROM
					"workgroup"
				WHERE
					"id" = '${req.body.workgroup_id}'
			`)

			if (workgroupInfo.manager === req.user.userId || (!_.isEmpty(workgroupInfo.submanager) && _.includes(workgroupInfo.submanager.split(','), req.user.userId))) {
				role.push('workgroup_manager')
			}
		}

		if (req.body.case_id) {
			const workgroupInfo = await db.one(`
				SELECT
					"workgroup".*
				FROM
					"case"
				LEFT JOIN
					"workgroup"
						ON
							"workgroup".id = "case".workgroup_id
				WHERE
					"case".id = '${req.body.case_id}'
			`)

			const caseInfo = await db.one(`
				SELECT
					*,
					ARRAY(
						SELECT
							user_id
						FROM
							"case_participant"
						WHERE
							case_id = '${req.body.case_id}'
					) AS "case_participant"
				FROM
					"case"
				WHERE
					id = '${req.body.case_id}'
			`)

			if (workgroupInfo.manager === req.user.userId || (!_.isEmpty(workgroupInfo.submanager) && _.includes(workgroupInfo.submanager.split(','), req.user.userId))) {
				role.push('workgroup_manager')
			}

			if (caseInfo.manager === req.user.userId) {
				role.push('case_manager')
			}

			if (_.includes(caseInfo.case_participant, req.user.userId) || caseInfo.issuer === req.user.userId) {
				role.push('case_participant')
			}
		}

		if (baseRole.role === 'administrator') {
			role.push('administrator')
		}

		const permit = await db.many(`
			SELECT
				*
			FROM
				"authorization"
		`)

		let result = _(permit)
			.groupBy('role')
			.pick(role)
			.reduce((res, value) => {
				value.map(v => {
					if (!res[v.name]) {
						res[v.name] = v.permit
					} else {
						res[v.name] = res[v.name] || v.permit
					}
				})
				return res
			}, {} as any)

		result = _.map(result, (r, k) => {
			return {
				name: k,
				permit: r
			}
		})

		response(res, result)
	} catch (e) {
		return next(e)
	}
}
