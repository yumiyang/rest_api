import { Request, Response } from 'express'
import * as jwt from '../../../config/jwt'
import { v4 as uuidv4 } from 'uuid'
import { validate as isEmail } from 'email-validator'
import crypto from 'crypto'
import axios from 'axios'
import xmlParser from 'fast-xml-parser'

import { defaultError, response } from '../../global/response'
import { db, pgp } from '../../../config/db'
import gwSync from '../../global/sync'

const zeropad = (inputData: string) => Buffer.concat([Buffer.from(inputData), Buffer.alloc(16 - inputData.length % 16, 0x00)]).toString()

function encrypt(dataString: string) {
	const cipher = crypto.createCipheriv('aes-128-ecb', process.env.GROUPWARE_ID_SECRET, null)
	cipher.setAutoPadding(false)
	let result = cipher.update(zeropad(dataString), 'utf8', 'base64')
	result += cipher.final('base64')
	return result
}

export const Login = async (req: Request, res: Response) => {
	try {
		const email = req.body.email
		const password = req.body.password
		const encryptedEmail = encodeURIComponent(encrypt(email))

		console.log(email)
		console.log(encryptedEmail)
		console.log(password)

		const result = await axios.get(`http://mgw.cncityenergy.com/ekp/service/openapi/IF_MSG_035_GetLogin?userId=${encryptedEmail}&password=${password}`)
		const resultJson = xmlParser.parse(result.data)

		console.log(resultJson)

		if (resultJson.loginCheck.result || resultJson.loginCheck.result === 'true' || resultJson.loginCheck.resultMsg === 1002) {
			const user = await db.one(`
				SELECT
					id, email, password, name, gwid, gwno
				FROM
					"user"
				WHERE
					gwno = '${email.toLowerCase()}' OR
					gwid = '${email.toLowerCase()}'
			`)
			const token = await jwt.sign({ email: user.email, userId: user.id, userGwid: user.gwid, userGwno: user.gwno })
			response(res, { token })
		} else {
			let whereQuery: string

			if (isEmail(email)) {
				whereQuery = `
					email = '${email}'
				`
			} else {
				whereQuery = `
					gwno = '${email.toLowerCase()}' OR
					gwid = '${email.toLowerCase()}'
				`
			}

			const user = await db.one(`
				SELECT
					id, email, password, name, gwid, gwno
				FROM
					"user"
				WHERE
					${whereQuery}
			`)

			if (password === user.password) {
				const token = await jwt.sign({ email: user.email, userId: user.id, userGwid: user.gwid, userGwno: user.gwno })
				response(res, { token })
			} else {
				defaultError(res, 'Invalid password')
			}
		}
	} catch (err) {
		console.log(err)
		defaultError(res, 'Failed Authentication')
	}
}

export const Register = async function (req: Request, res: Response) {
	req.body.id = uuidv4()

	try {
		await db.none(pgp.helpers.insert(req.body, null, 'user'))

		const token = jwt.sign({ email: req.body.email })

		response(res, { token })
	} catch (err) {
		defaultError(res, err)
	}
}

export const Me = async function (req: Request, res: Response) {
	try {
		const userInfo = await db.one(`
			SELECT
				"id", "email", "name", "photo", "phone", "company", "position", "department",
				"last_login", "connected", "role", "gwid", "gwno"
			FROM
				"user"
			WHERE
				"user".id = '${req.user.userId}'
		`)

		response(res, userInfo)
	} catch (err) {
		defaultError(res, err)
	}
}

export const Sync = async function (req: Request, res: Response) {
	try {
		const userInfo = await db.one(`
			SELECT
				"id", "email", "name", "photo", "phone", "company", "position", "department",
				"last_login", "connected", "role", "gwid", "gwno"
			FROM
				"user"
			WHERE
				"user".id = '${req.user.userId}'
		`)

		if (userInfo.role !== 'administrator') {
			defaultError(res, 'Unauthorized')
		} else {
			await gwSync()
			response(res)
		}
	} catch (err) {
		defaultError(res, err)
	}
}

export const Link = async function (req: Request, res: Response) {
	try {
		const cryptedEmpId = decodeURIComponent(req.body.emp_id)

		const decipher = crypto.createDecipheriv('aes-128-ecb', process.env.GROUPWARE_ID_SECRET, null)
		decipher.setAutoPadding(false)

		let emp_id = decipher.update(cryptedEmpId, 'base64', 'utf8')
		emp_id = emp_id.replace(/\u0000/g, '')

		const user = await db.oneOrNone(`
			SELECT
				id, email, password, name, gwid, gwno
			FROM
				"user"
			WHERE
				gwid = '${emp_id}' OR
				gwno = '${emp_id}'
		`)

		if (user) {
			const token = await jwt.sign({ email: user.email, userId: user.id, userGwid: user.gwid, userGwno: user.gwno })
			response(res, { token })
		} else {
			throw new Error('Invalid emp_id')
		}
	} catch (err) {
		console.log(err)
		defaultError(res, err.message)
	}
}