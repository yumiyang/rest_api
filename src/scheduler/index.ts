import fs from 'fs'
import path from 'path'

import logger from '../config/logger'

export default async function () {
  const jobs = fs
    .readdirSync(path.join(__dirname, 'job'))
    .filter(file => !/.*(\.d\.).*$/.test(file))

  jobs.forEach(job => {
    require(path.join(__dirname, 'job', job))
    logger.info(`스케쥴 [${(job.match(/(.*)\.[^.]{1,10}$/) as string[])[1]}] 로드 완료`)
  })
  logger.info('스케쥴러 초기화 완료')
}
