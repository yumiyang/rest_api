import schedule from 'node-schedule'
import path from 'path'
import _ from 'lodash'

import { db } from '../../config/db'
import logger from '../../config/logger'
import Event from '../../socket/lib/event'

const scheduleName = `[${(path.basename(__filename).match(/(.*)\.[^.]{1,10}$/) as string[])[1]}]`

schedule.scheduleJob('0 9 * * *', async () => {
  logger.verbose(`${scheduleName} 매일 09시 스케쥴 실행`)

  const expiredVote = await db.manyOrNone(`
    SELECT DISTINCT
      case_id
    FROM
      "vote"
    WHERE
      DATE_TRUNC('day', expired_at) <= DATE_TRUNC('day', NOW())
  `)

  if (!_.isEmpty(expiredVote)) {
    await db.none(`
      DELETE FROM
        "vote"
      WHERE
        case_id IN (${expiredVote.map(c => `'${c.case_id}'`)})
    `)

    expiredVote.map(async expiredCase => {
      await db.none(`
        UPDATE
          "case"
        SET
          "status" = '종료',
          "close_time" = NOW(),
          "progress" = 100
        WHERE
          "id" = '${expiredCase.case_id}'
      `)

      const event = new Event(expiredCase.case_id, 'case.end', 'system', '', expiredCase.case_id)
      await event.save()
      await event.send()
    })
  }
})