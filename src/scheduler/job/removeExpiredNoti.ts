import schedule from 'node-schedule'
import path from 'path'

import { db } from '../../config/db'
import logger from '../../config/logger'

const scheduleName = `[${(path.basename(__filename).match(/(.*)\.[^.]{1,10}$/) as string[])[1]}]`

schedule.scheduleJob('0 * * * *', async () => {
  logger.verbose(`${scheduleName} 매 시간 스케쥴 실행`)

  await db.none(`
    UPDATE
      "notification"
    SET
      checked = NOW()
    WHERE
      timestamp < NOW() - INTERVAL '7 days' AND
      checked IS NOT NULL
  `)
})