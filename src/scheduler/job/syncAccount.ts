import schedule from 'node-schedule'
import path from 'path'

import logger from '../../config/logger'
import Sync from '../../api/global/sync'

const scheduleName = `[${(path.basename(__filename).match(/(.*)\.[^.]{1,10}$/) as string[])[1]}]`

schedule.scheduleJob({ hour: 0, minute: 40 }, async () => {
    logger.verbose(`${scheduleName} 매일 00시 40분 스케쥴 실행`)
    await Sync()
})

schedule.scheduleJob({ hour: 12, minute: 40 }, async () => {
    logger.verbose(`${scheduleName} 매일 12시 40분 스케쥴 실행`)
    await Sync()
})