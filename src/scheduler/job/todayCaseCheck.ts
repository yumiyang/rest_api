import schedule from 'node-schedule'
import path from 'path'

import { db } from '../../config/db'
import logger from '../../config/logger'
import Event from '../../socket/lib/event'

const scheduleName = `[${(path.basename(__filename).match(/(.*)\.[^.]{1,10}$/) as string[])[1]}]`

schedule.scheduleJob('0 9 * * *', async () => {
  logger.verbose(`${scheduleName} 매일 09시 스케쥴 실행`)

  const targetCase = await db.manyOrNone(`
    SELECT
      *
    FROM
      "case"
    WHERE
      DATE_TRUNC('day', target_date) = DATE_TRUNC('day', NOW())
  `)

  targetCase.map(async c => {
    const event = new Event(c.manager, 'case.end.agreement', 'system', '', c.id)
    await event.save()
    await event.send()
  })

  const requestCase = await db.manyOrNone(`
    SELECT
      *
    FROM
      "case"
    WHERE
      DATE_TRUNC('day', request_date) = DATE_TRUNC('day', NOW())
  `)

  requestCase.map(async c => {
    const event = new Event(c.manager, 'case.end.agreement', 'system', '', c.id)
    await event.save()
    await event.send()
  })
})