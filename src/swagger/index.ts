import swaggerJSDoc from 'swagger-jsdoc'

export default swaggerJSDoc({
  swaggerDefinition: {
    info: {
      title: 'WORKS API',
      version: '0.1.0'
    },
    host: process.env.HOST,
    basePath: '/',
    contact: {
      email: 'we@companywe.co.kr'
    },
    securityDefinitions: {
      jwt: {
        type: 'apiKey',
        name: 'Authorization',
        schema: 'bearer',
        bearerFormat: 'Bearer',
        in: 'header'
      }
    },
    security: [
      { jwt: [] }
    ],
  },
  components: {
    schemas: {
      Case: {
        type: 'object',
        properties: {
          name: {
            type: 'string'
          },
          status: {
            type: 'string'
          },
          start_date: {
            type: 'date'
          },
          end_date: {
            type: 'date'
          },
          target_date: {
            type: 'date'
          },
          request_date: {
            type: 'date'
          },
          progress: {
            type: 'number'
          },
          issuer: {
            type: 'string'
          }
        }
      }
    },
    res: {
      BadRequest: {
        description: '잘못된 요청.',
        schema: {
          $ref: `#/components/errorResult/Error`
        }
      },
      UnAuthorized: {
        description: '권한이 없습니다.',
        schema: {
          $ref: `#/components/errorResult/Error`
        }
      },
      Forbidden: {
        description: '잘못된 접근입니다.',
        schema: {
          $ref: `#/components/errorResult/Error`
        }
      },
      NotFound: {
        description: '없는 리소스 요청.',
        schema: {
          $ref: `#/components/errorResult/Error`
        }
      }
    },
    errorResult: {
      Error: {
        type: 'object',
        properties: {
          error: {
            type: 'string',
            description: '에러 메시지'
          }
        }
      }
    }
  },
  apis: [__dirname + '/../api/routes/**/*.*']
})
