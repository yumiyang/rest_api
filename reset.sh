#!/bin/sh

SHELL_PATH=`pwd -P`
DB="works"
USER="works"

export PGPASSWORD=8660works1!

dropdb ${DB}
createdb ${DB} -O ${USER}

psql -U ${USER} -d ${DB} -f "${SHELL_PATH}/sql/structure.sql"
psql -U ${USER} -d ${DB} -f "${SHELL_PATH}/sql/seed.sql"