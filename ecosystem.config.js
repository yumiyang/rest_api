require('dotenv').config()
module.exports = {
  apps: [
    {
      name: process.env.APP_ID,
      script: 'npm',
      args: 'run deploy',
      log_date_format: 'YYYY-MM-DD HH:mm:ss Z',
      env: {
        TZ: 'Asia/Seoul'
      }
    }
  ],
  deploy: {
    development: {
      user: process.env.DEVELOPMENT_SERVER_USER,
      host: process.env.DEVELOPMENT_SERVER_HOST,
      repo: `git@gitlab.companywe.co.kr:${process.env.PROJECT_ID}/${process.env.APP_ID}.git`,
      ref: `origin/${process.env.PRODUCTION_BRANCH}`,
      path: `/home/ubuntu/${process.env.APP_ID}`,
      'pre-deploy-local': `scp .env.development ${process.env.DEVELOPMENT_SERVER_USER}@${process.env.DEVELOPMENT_SERVER_HOST}:/home/ubuntu/${process.env.APP_ID}/current/.env`,
      'post-deploy': `npm install && pm2 startOrRestart ecosystem.config.js --update-env`
    },
    production: {
      user: process.env.PRODUCTION_SERVER_USER,
      host: process.env.PRODUCTION_SERVER_HOST,
      repo: `git@gitlab.companywe.co.kr:${process.env.PROJECT_ID}/${process.env.APP_ID}.git`,
      ref: `origin/${process.env.PRODUCTION_BRANCH}`,
      path: `/home/ubuntu/${process.env.APP_ID}`,
      'pre-deploy-local': `scp .env.production ${process.env.PRODUCTION_SERVER_USER}@${process.env.PRODUCTION_SERVER_HOST}:/home/ubuntu/${process.env.APP_ID}/current/.env`,
      'post-deploy': `npm install && pm2 startOrRestart ecosystem.config.js --update-env`
    }
  }
}
