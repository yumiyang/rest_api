module.exports = {
  env: {
    es2021: true,
    node: true
  },
  extends: ['standard', 'plugin:prettier/recommended', 'prettier/@typescript-eslint'],
  parser: ['@typescript-eslint/parser'],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    project: './tsconfig.json'
  },
  ignorePatterns: ['node_modules', 'build'],
  plugins: ['prettier'],
  rules: {
    'prettier/prettier': 'error'
  }
}
