require('dotenv').config()

const mariadb = require('mariadb')
const _ = require('lodash')
const uuid = require('uuid')
const fs = require('fs')
const axios = require('axios')

async function main() {
  const pool = mariadb.createPool({
    host: process.env.GW_DATABASE_HOST,
    user: process.env.GW_DATABASE_USER,
    password: process.env.GW_DATABASE_PASS,
    database: process.env.GW_DATABASE_DB
  })

  let conn;

  try {
    conn = await pool.getConnection()

    const users = await conn.query(`
        SELECT
            *
        FROM
            v_insa_user
    `)

    const job = await conn.query(`
        SELECT
            *
        FROM
            v_insa_my_job
    `)

    const office = await conn.query(`
        SELECT
            *
        FROM
            v_insa_office
    `)

    const dept = await conn.query(`
        SELECT
            *
        FROM
            v_insa_dept
    `)

    const duty = await conn.query(`
        SELECT
            *
        FROM
            v_insa_duty
    `)

    const position = await conn.query(`
        SELECT
            *
        FROM
            v_insa_position
    `)

    fs.writeFileSync('users.json', JSON.stringify(users, null, 4))
    fs.writeFileSync('job.json', JSON.stringify(job, null, 4))
    fs.writeFileSync('office.json', JSON.stringify(office, null, 4))
    fs.writeFileSync('dept.json', JSON.stringify(dept, null, 4))
    fs.writeFileSync('duty.json', JSON.stringify(duty, null, 4))
    fs.writeFileSync('position.json', JSON.stringify(position, null, 4))

    console.log('Job Done.')
  } catch (err) {
    throw err
  } finally {
    if (conn) return conn.end()
  }
}

main()