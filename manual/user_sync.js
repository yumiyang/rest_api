require('dotenv').config()

const mariadb = require('mariadb')
const _ = require('lodash')
const uuid = require('uuid')

const pgPromise = require('pg-promise')

const pgp = pgPromise({
  capSQL: true,
  query: e => console.log(e.query),
  error: e => {
    console.log(e)
  }
})

const db = pgp(process.env.DATABASE_URL)

async function main() {
  const pool = mariadb.createPool({
    host: process.env.GW_DATABASE_HOST,
    user: process.env.GW_DATABASE_USER,
    password: process.env.GW_DATABASE_PASS,
    database: process.env.GW_DATABASE_DB
  })

  let conn;

  try {
    conn = await pool.getConnection()

    const rows = await conn.query(`
      SELECT
        v_insa_user.*,
        v_insa_my_job.dept_name AS department,
        v_insa_office.ofc_name AS office
      FROM
        v_insa_my_job
      LEFT JOIN
        v_insa_user
          ON LOWER(v_insa_my_job.emp_no) = LOWER(v_insa_user.emp_no)
      LEFT JOIN
        v_insa_office
          ON v_insa_office.ofc_id = v_insa_user.ofc_id
      WHERE
        v_insa_my_job.bass_dept_yn = 'Y' AND
        v_insa_user.ofc_id NOT LIKE 'DELETE%'
    `)

    await Promise.all(_.map(rows, async user => {
      try {
        await db.none(`
          INSERT INTO
            "user"
            ("id", "company", "name", "gwid", "gwno", "email", "photo", "department", "position", "phone", "role")
          VALUES
            ('${uuid.v4()}', '${user.CMP_NAME}', '${user.USER_NAME}', '${user.EMP_ID}', '${user.EMP_NO}', '${user.CMP_EMAIL}', '${user.PROFILE_IMG}', '${user.department}', '${user.office}', '${user.CELL_PHONE}', 'user')
          ON CONFLICT ON CONSTRAINT unique_gwid
            DO UPDATE
            SET ("company", "name", "email", "photo", "department", "position", "phone", "gwno") = (EXCLUDED."company", EXCLUDED."name", EXCLUDED."email", EXCLUDED."photo", EXCLUDED."department", EXCLUDED."position", EXCLUDED."phone", EXCLUDED."gwno");
        `)
      } catch (error) {
        console.log(error)
      }
    }))

    await db.none(`
      INSERT INTO
        "user"
        ("id", "company", "name", "gwid", "password", "email", "role")
      VALUES
        ('${uuid.v4()}', '컴퍼니위', '관리자', 'h00000', 'TMG1h%2FQ8SpDmK7hS7VNb%2BA%3D%3D', 'admin@companywe.co.kr', 'administrator')
      ON CONFLICT ON CONSTRAINT unique_gwid
        DO NOTHING;
    `)

    console.log('DB Inserted')
  } catch (err) {
    throw err
  } finally {
    if (conn) return conn.end()
  }
}

main()