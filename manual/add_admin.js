require('dotenv').config()

const uuid = require('uuid')
const pgPromise = require('pg-promise')

const pgp = pgPromise({
  capSQL: true,
  query: e => {
    console.log(e.query)
  }
})

const db = pgp(process.env.DATABASE_URL)

async function main() {
  try {
    await db.none(`
      INSERT INTO
        "user"
        ("id", "company", "name", "gwid", "password", "email", "role")
      VALUES
        ('${uuid.v4()}', '컴퍼니위', '마크_유저', 'h00001', 'TMG1h%2FQ8SpDmK7hS7VNb%2BA%3D%3D', 'mark@companywe.co.kr', 'user')
      ON CONFLICT ON CONSTRAINT user_gwid_key
        DO NOTHING;
    `)

    await db.none(`
      INSERT INTO
        "user"
        ("id", "company", "name", "gwid", "password", "email", "role")
      VALUES
        ('${uuid.v4()}', '컴퍼니위', '마크_유저2', 'h00002', 'TMG1h%2FQ8SpDmK7hS7VNb%2BA%3D%3D', 'mark.jung@companywe.co.kr', 'user')
      ON CONFLICT ON CONSTRAINT user_gwid_key
        DO NOTHING;
    `)

    console.log('DB Inserted')
  } catch (err) {
    throw err
  }
}

main()