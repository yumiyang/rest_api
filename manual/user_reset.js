require('dotenv').config()

const mariadb = require('mariadb')
const _ = require('lodash')
const uuid = require('uuid')

const pgPromise = require('pg-promise')

const pgp = pgPromise({
  capSQL: true,
  query: e => {
    console.log(e.query)
  },
  error: e => {
    console.log(e)
  }
})

const db = pgp(process.env.DATABASE_URL)

async function main() {
  const pool = mariadb.createPool({
    host: process.env.GW_DATABASE_HOST,
    user: process.env.GW_DATABASE_USER,
    password: process.env.GW_DATABASE_PASS,
    database: process.env.GW_DATABASE_DB
  })

  let conn;

  try {
    await db.none(`
      DROP TABLE
        "user"
      CASCADE
    `)

    await db.none(`
      CREATE TABLE "public"."user" (
        "id" uuid NOT NULL,
        "email" text NOT NULL PRIMARY KEY,
        "password" text,
        "name" text,
        "gwid" text UNIQUE,
        "photo" text,
        "phone" text,
        "company" text,
        "position" text,
        "department" text,
        "last_login" timestamptz(6),
        "connected" bool,
        "is_groupware" bool,
        "role" text,
        "created_at" timestamptz(6) DEFAULT now()
      )
    `)

    conn = await pool.getConnection()

    const rows = await conn.query(`
      SELECT
        v_insa_user.*,
        v_insa_my_job.dept_name AS department,
        v_insa_office.ofc_name AS office
      FROM
        v_insa_my_job
      LEFT JOIN
        v_insa_user
          ON LOWER(v_insa_my_job.emp_no) = LOWER(v_insa_user.emp_no)
      LEFT JOIN
        v_insa_office
          ON v_insa_office.ofc_id = v_insa_user.ofc_id
      WHERE
        v_insa_my_job.bass_dept_yn = 'Y' AND
        v_insa_my_job.dept_id NOT LIKE 'DELETE%'
    `)

    await Promise.all(_.map(rows, async user => {
      await db.none(`
        INSERT INTO
          "user"
          ("id", "company", "name", "gwid", "email", "photo", "department", "position", "phone", "role", "password")
        VALUES
          ('${uuid.v4()}', '${user.CMP_NAME}', '${user.USER_NAME}', '${user.EMP_NO}', '${user.CMP_EMAIL}', '${user.PROFILE_IMG}', '${user.department}', '${user.office}', '${user.CELL_PHONE}', 'user', '74dbd6acf6e1a4f398270e4cabb583ed')
        ON CONFLICT ON CONSTRAINT user_gwid_key
          DO NOTHING;
      `)
    }))

    await db.none(`
      INSERT INTO
        "user"
        ("id", "company", "name", "gwid", "password", "email", "role")
      VALUES
        ('${uuid.v4()}', '컴퍼니위', '관리자', 'h00000', 'TMG1h%2FQ8SpDmK7hS7VNb%2BA%3D%3D', 'admin@companywe.co.kr', 'administrator')
      ON CONFLICT ON CONSTRAINT user_gwid_key
        DO NOTHING;
    `)

    console.log('DB Inserted')
  } catch (err) {
    console.log(err)
  } finally {
    if (conn) return conn.end()
  }
}

main()