INSERT INTO "role"("name", "name_kor") VALUES ('administrator', '관리자');
INSERT INTO "role"("name", "name_kor") VALUES ('user', '사용자');
INSERT INTO "role"("name", "name_kor") VALUES ('guest', '게스트');
INSERT INTO "role"("name", "name_kor") VALUES ('case_manager', '요청사항 처리자');
INSERT INTO "role"("name", "name_kor") VALUES ('workgroup_manager', '단위업무 처리자');
INSERT INTO "role"("name", "name_kor") VALUES ('case_participant', '요청사항 참여자');

INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_create', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_create', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_create', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_create', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_create', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_access', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_access', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_access', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_access', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_access', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_update', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_update', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_update', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_update', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_update', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_close', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_close', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_close', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_close', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_close', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_chat', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_chat', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_chat', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_chat', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_chat', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_delete_request', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_delete_request', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_delete_request', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_delete_request', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_delete_request', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_delete_approve', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_delete_approve', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_delete_approve', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_delete_approve', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_delete_approve', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_restore', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_restore', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_restore', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_restore', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_restore', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_attachment', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_attachment', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_attachment', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_attachment', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_attachment', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_invite', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_invite', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_invite', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_invite', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_invite', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_transfer', 'administrator');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_transfer', 'workgroup_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_transfer', 'case_manager');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_transfer', 'case_participant');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_create', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('t', 'case_access', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_update', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_close', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_chat', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_delete_request', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_delete_approve', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_restore', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_attachment', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_invite', 'guest');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_transfer', 'user');
INSERT INTO "authorization"("permit", "name", "role") VALUES ('f', 'case_transfer', 'guest');

INSERT INTO "customer"("id", "name", "description", "created_at") VALUES (0, 'CNCITY에너지', NULL, NOW());

INSERT INTO "section"("id", "name", "customer_id", "description") VALUES (0, 'IT 서비스', 0, NULL);

INSERT INTO "category"("id", "name", "description", "section_id") VALUES (0, '서비스 데스크', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('SAP', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('그룹웨어', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('HR', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('고객서비스센터', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('안전관리', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('전자세금계산서', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('홈페이지', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('협력업체포탈', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ('내부회계', NULL, 0);
INSERT INTO "category"("name", "description", "section_id") VALUES ( '인프라', NULL, 0);

INSERT INTO "workgroup"("id", "category_id", "name", "manager", "submanager", "description") VALUES (0, 0, '서비스 데스크', NULL, NULL, '안녕하세요 - 서비스 데스크입니다 ');
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '요금관리(CCS)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '영업/공사관리(CMP/ECP)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '자재관리(SD/MM)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '안전점검', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '회계(FI)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '인프라(BC)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '인터페이스(PI)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (1, '기타', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (2, '전자결재', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (2, '메일', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (2, '메신저', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (2, '모바일앱', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (2, '기타', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (3, 'HR', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (4, '포탈', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (4, '모바일', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (5, '스마트현장지원(S1)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (5, '배관망해석', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (5, '위기관리시스템', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (5, 'SCADA', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (5, '기타', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (6, '세금계산서', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (6, '기타', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (7, 'PC홈페이지', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (7, '모바일앱', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (8, '협력업체포탈', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (9, '내부회계', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (10, '네트워크', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (10, '보안', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (10, '장비(PC/태블릿 등)', NULL, NULL, NULL);
INSERT INTO "workgroup"("category_id", "name", "manager", "submanager", "description") VALUES (10, 'AWS Cloud', NULL, NULL, NULL);

INSERT INTO "user"("id", "email", "password", "name", "photo", "phone", "company", "position", "department", "last_login", "connected", "is_groupware", "role", "created_at") VALUES ('fac27f23-4bc7-4266-a1cc-da1d9eb80310', 'admin@cncity.com', '74dbd6acf6e1a4f398270e4cabb583ed', '관리자', NULL, NULL, 'CNCITY', NULL, NULL, NULL, 'f', NULL, 'administrator', NOW());

INSERT INTO "chatbot_answer"("type", "label", "text", "id") VALUES ('etc', '기타 문의사항', '기타 문의사항은 채팅창에 메시지 남겨주시거나 메일(works@cncity.co.kr) 및 대표번호(1522-7835)로 회신바랍니다.
감사합니다.!', 1);
INSERT INTO "chatbot_answer"("type", "label", "text", "id") VALUES ('remote', '원격접속 신청', '안녕하세요, 서비스데스크입니다.
원격접속 신청 승인해드렸습니다.
기타 문의사항은 채팅창에 메시지 남겨주시거나 메일(works@cncity.co.kr) 및 대표번호(1522-7835)로 회신바랍니다.
감사합니다.', 2);
INSERT INTO "chatbot_answer"("type", "label", "text", "id") VALUES ('checkRequest', '요청확인', '안녕하세요, 서비스데스크입니다.
문의주신 요청사항은 해당 담당자에게 전달하였습니다.
해당 담당자를 통해 요청결과가 회신 될 예정이오니, 기다려 주시면 감사하겠습니다.
기타 문의사항은 채팅창에 메시지 남겨주시거나 메일(works@cncity.co.kr) 및 대표번호(1522-7835)로 회신바랍니다.
감사합니다.', 3);
INSERT INTO "chatbot_answer"("type", "label", "text", "id") VALUES ('hello', '인사', '안녕하세요, 서비스데스크입니다.
무엇을 도와드릴까요?', 4);
